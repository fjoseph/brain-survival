package com.Fawaz.Server.Main;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.Fawaz.Server.Mob.Player;
import com.Fawaz.Server.Mob.Tuple;
import com.Fawaz.Server.Packets.DiscPacket;
import com.Fawaz.Server.Packets.LoginPacket;
import com.Fawaz.Server.Packets.LoginResponsePacket;
import com.Fawaz.Server.Packets.PlayerInformationPacket;
import com.Fawaz.Server.Packets.PrivateRoomInformation;
import com.Fawaz.Server.Packets.RoomRequestPacket;
import com.Fawaz.Server.Packets.StartPacket;
import com.Fawaz.Server.Packets.InGamePackets.BlindPacket;
import com.Fawaz.Server.Packets.InGamePackets.ButtonPressPacket;
import com.Fawaz.Server.Packets.InGamePackets.ChangeGamePacket;
import com.Fawaz.Server.Packets.InGamePackets.ColorMatchingImagePacket;
import com.Fawaz.Server.Packets.InGamePackets.CupGameStatusPacket;
import com.Fawaz.Server.Packets.InGamePackets.GameOverPacket;
import com.Fawaz.Server.Packets.InGamePackets.InfluencePacket;
import com.Fawaz.Server.Packets.InGamePackets.LabelColorPacket;
import com.Fawaz.Server.Packets.InGamePackets.MiddleButtonPacket;
import com.Fawaz.Server.Packets.InGamePackets.SimonSaysPacket;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Listener.ThreadedListener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

public class ServerProgram {

	Server server;
	private int version = 3;
	private ConcurrentLinkedQueue<Player> clientPool = new ConcurrentLinkedQueue<Player>();
	private ConcurrentHashMap<Connection, Player> clientMap = new ConcurrentHashMap<Connection, Player>();
	private ExecutorService threadpool = Executors.newCachedThreadPool();
	private ConcurrentLinkedQueue<Tuple> tuplePool = new ConcurrentLinkedQueue<Tuple>();
	private ConcurrentHashMap<Integer, Tuple> privateRooms = new ConcurrentHashMap<Integer, Tuple>(
			10, .9f, 4);
	private Random rand;

	public ServerProgram() {
		server = new Server();
		rand = new Random();
		server.getKryo().register(ButtonPressPacket.class);
		server.getKryo().register(ChangeGamePacket.class);
		server.getKryo().register(ColorMatchingImagePacket.class);
		server.getKryo().register(ColorMatchingImagePacket[].class);
		server.getKryo().register(CupGameStatusPacket.class);
		server.getKryo().register(InfluencePacket.class);
		server.getKryo().register(GameOverPacket.class);
		server.getKryo().register(MiddleButtonPacket.class);
		server.getKryo().register(SimonSaysPacket.class);
		server.getKryo().register(int[].class);
		server.getKryo().register(LoginPacket.class);
		server.getKryo().register(LoginResponsePacket.class);
		server.getKryo().register(RoomRequestPacket.class);
		server.getKryo().register(StartPacket.class);
		server.getKryo().register(PlayerInformationPacket.class);
		server.getKryo().register(PlayerInformationPacket[].class);
		server.getKryo().register(DiscPacket.class);
		server.getKryo().register(LabelColorPacket.class);
		server.getKryo().register(BlindPacket.class);
		server.getKryo().register(PrivateRoomInformation.class);
		// register Packets

	}

	public void init() {
		try {
			server.bind(new InetSocketAddress("23.239.17.61", 55555),
					new InetSocketAddress("23.239.17.61", 55555));
			// server.bind(new InetSocketAddress("192.168.8.107", 55555),
			// new InetSocketAddress("192.168.8.107", 55555));
		} catch (IOException e) {
			e.printStackTrace();
		}

		server.addListener(new ServerListener(new Listener(), threadpool));
		server.start();
	}

	public static void main(String[] args) {
		ServerProgram server = new ServerProgram();
		server.init();
	}

	private class ServerListener extends ThreadedListener {

		public ServerListener(Listener arg0, ExecutorService arg1) {
			super(arg0, arg1);

		}

		@Override
		public void received(Connection connection, Object object) {
			Log.info("Recieved", object.getClass().getSimpleName());
			if (object instanceof LoginPacket) {
				LoginPacket lp = (LoginPacket) object;
				if (version != lp.version) {
					LoginResponsePacket lrp = new LoginResponsePacket();
					lrp.accepted = false;
					lrp.Message = "Wrong Version\nPlease update the app";
					connection.sendTCP(lrp);
				} else {
					connection.setKeepAliveTCP(2000);
					connection.setTimeout(5000);
					Player p = clientPool.poll();
					if (p == null) {
						p = new Player();
					}
					p.setConnection(connection);
					clientMap.put(connection, p);
					LoginResponsePacket lrp = new LoginResponsePacket();
					lrp.accepted = true;
					connection.sendTCP(lrp);
				}

			} else if (object instanceof RoomRequestPacket) {
				Log.info("Processing RoomRequest");
				RoomRequestPacket rrp = (RoomRequestPacket) object;
				Player player = clientMap.get(connection);
				if (player != null) {
					player.initialPersonalInformation(rrp.name, false, rrp.won,
							rrp.lost);
					Tuple room = null;
					if (rrp.RoomType == 1) {
						Log.debug("Tuplepool",
								"Size of pool: " + tuplePool.size());
						room = tuplePool.peek();
						if (room == null) {
							Log.debug("Room", "Room has to be created ");
							room = new Tuple();
							tuplePool.add(room);
						}

						if (room.addPlayer(player)) {
							tuplePool.remove(room);
							room.setInQueue(false);
						}
						room.setType(1);
					} else if (rrp.RoomType == 2) {
						PrivateRoomInformation pri = new PrivateRoomInformation();
						pri.id = makePrivateRoom();
						// pri.id = privateRooms.
						room = privateRooms.get(pri.id);
						if (room.addPlayer(player)) {
							tuplePool.remove(room);
							room.setInQueue(false);
						}
						room.setType(pri.id);
						connection.sendTCP(pri);
					} else {
						room = privateRooms.get(rrp.RoomType);
						if (room == null) {
							LoginResponsePacket lrp = new LoginResponsePacket();
							lrp.accepted = false;
							lrp.Message = "No room with that id\ntry again";
							connection.sendTCP(lrp);
							return;
						} else {
							if (room.addPlayer(player)) {
								room.setInQueue(false);
							}
							PrivateRoomInformation pri = new PrivateRoomInformation();
							pri.id = rrp.RoomType;
							connection.sendTCP(pri);
						}
					}
					sendInformationtoRoom(room);
					sendStart(room);

				}

			} else if (object instanceof PlayerInformationPacket) {
				// RoomInformationPacket r = (RoomInformationPacket) object;
				PlayerInformationPacket pi = (PlayerInformationPacket) object;
				Player player = clientMap.get(connection);
				if (player != null) {
					Tuple room = player.getRoom();
					if (room.isColorAvailable(pi.color))
						player.setColor(pi.color);
					player.setReady(pi.ready);
					sendInformationtoRoom(room);
					sendStart(room);
				}
			} else if (object instanceof ChangeGamePacket
					|| object instanceof MiddleButtonPacket
					|| object instanceof ButtonPressPacket
					|| object instanceof LabelColorPacket
					|| object instanceof ColorMatchingImagePacket[]
					|| object instanceof SimonSaysPacket
					|| object instanceof CupGameStatusPacket
					|| object instanceof BlindPacket
					|| object instanceof InfluencePacket
					|| object instanceof GameOverPacket) {
				Player p = clientMap.get(connection);
				if (p != null) {
					if (object instanceof ColorMatchingImagePacket)
						sendtoAllExceptUDP(p, object);
					else
						sendtoAllExceptTCP(p, object);
					if (object instanceof GameOverPacket) {
						p.getRoom().removePlayer(p);
						connection.close();
					}
				}
			}

		}

		@Override
		public void disconnected(Connection connection) {
			Player player = clientMap.remove(connection);
			if (player != null) {
				Tuple room = player.getRoom();
				if (room != null) {
					room.removePlayer(player);
					if (room.isInGame()) {
						DiscPacket dp = new DiscPacket();
						for (Player p : room.getPlayers())
							p.getConnection().sendTCP(dp);
						room.reset();
					} else
						sendInformationtoRoom(room);
					switch (room.getType()) {
					default:
						privateRooms.remove(room.getType());
						room.setInQueue(false);
					case 1:
						Log.debug("adding to the Pool");
						if (!room.isInQueue()) {
							room.setInQueue(true);
							tuplePool.add(room);
						}
						Log.debug("Size of pool: " + tuplePool.size());
						break;

					}
				}
				clientPool.add(player.reset());
			}

		}

		@Override
		public void connected(Connection connection) {
			connection.setTimeout(7000);
			connection.setKeepAliveTCP(4000);
		}

	}

	public int makePrivateRoom() {
		int roomId = rand.nextInt((99999 - 10000) + 1) + 10000;
		Tuple room;
		room = new Tuple();
		Tuple posConflict = privateRooms.putIfAbsent(roomId, room);
		if (posConflict != null) {
			room.reset();
			tuplePool.add(room);
			return makePrivateRoom();
		} else {
			return roomId;
		}

	}

	public void sendInformationtoRoom(Tuple room) {

		PlayerInformationPacket[] pi = new PlayerInformationPacket[room.size()];
		for (int i = 0; i < pi.length; i++)
			pi[i] = new PlayerInformationPacket();

		int i = 0;
		for (Player p : room.getPlayers()) {
			pi[i].Id = p.getConnection().getID();
			pi[i].color = p.getColor();
			pi[i].ready = p.isReady();
			pi[i].username = p.getUsername();
			pi[i].won = p.getWon();
			pi[i].lost = p.getLost();
			i++;
		}

		for (Player p : room.getPlayers())
			p.getConnection().sendTCP(pi);

	}

	public void sendStart(Tuple room) {
		StartPacket sp = new StartPacket();
		sp.start = room.isReady();
		for (Player p : room.getPlayers()) {
			if (sp.start) {
				float startTime = 5000 - p.getConnection().getReturnTripTime() / 2;
				sp.estimatedTime = startTime / 1000;
			}
			room.setInGame(sp.start);
			p.getConnection().sendTCP(sp);
		}
	}

	private void sendtoAllExceptTCP(Player player, Object object) {
		Tuple room = player.getRoom();
		for (Player p : room.getPlayers()) {
			if (!player.equals(p))
				p.getConnection().sendTCP(object);
		}
	}

	private void sendtoAllExceptUDP(Player player, Object object) {
		Tuple room = player.getRoom();
		for (Player p : room.getPlayers()) {
			if (!player.equals(p))
				p.getConnection().sendUDP(object);
		}
	}

}
