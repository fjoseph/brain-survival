package com.Fawaz.Server.Mob;

import com.esotericsoftware.kryonet.Connection;

public class Player {

	private Connection connection;
	private String username;
	private int color = -1;
	private boolean ready = false;
	private int won = 0;
	private int lost = 0;
	private Tuple room = null;

	public Player() {
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void initialPersonalInformation(String username, boolean ready,
			int won, int lost) {
		this.username = username;
		this.ready = ready;
		this.won = won;
		this.lost = lost;
	}

	public int getWon() {
		return won;
	}

	public int getLost() {
		return lost;
	}

	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}

	public Connection getConnection() {
		return connection;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public Tuple getRoom() {
		return room;
	}

	public void setRoom(Tuple room) {
		this.room = room;
	}

	public String getUsername() {
		return username;
	}

	public Player reset() {
		connection = null;
		color = -1;
		room = null;
		won = 0;
		lost = 0;
		ready = false;
		return this;
	}

}
