package com.Fawaz.Server.Mob;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.esotericsoftware.minlog.Log;

public class Tuple {

	protected ConcurrentLinkedQueue<Player> playersinRoom;
	private boolean inQueue;
	private static Random random = new Random();
	private boolean inGame = false;
	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Tuple() {
		playersinRoom = new ConcurrentLinkedQueue<Player>();
		inQueue = true;
		inGame = false;
	}

	public boolean addPlayer(Player player) {
		player.setRoom(this);
		player.setColor(getFirstColor());
		playersinRoom.add(player);
		Log.debug("Room", "Size of room: " + playersinRoom.size());
		return isFull();
	}

	public int getFirstColor() {
		int firstColor = random.nextInt(8);
		if (isColorAvailable(firstColor))
			return firstColor;
		else
			return getFirstColor();

	}

	public boolean isColorAvailable(int color) {
		for (Player p : playersinRoom) {
			if (p.getColor() == color)
				return false;
		}
		return true;
	}

	public boolean isFull() {
		return playersinRoom.size() == 2;
	}

	public void reset() {
		playersinRoom.clear();
		inQueue = true;
		inGame = false;
	}

	public ConcurrentLinkedQueue<Player> getPlayers() {
		return playersinRoom;
	}

	public boolean removePlayer(Player player) {
		playersinRoom.remove(player);
		return playersinRoom.size() == 0;
	}

	public boolean isInQueue() {
		return inQueue;
	}

	public void setInQueue(boolean inQueue) {
		this.inQueue = inQueue;
	}

	public int size() {
		return playersinRoom.size();
	}

	public boolean isReady() {
		if (isFull()) {
			for (Player p : playersinRoom) {
				if (!p.isReady())
					return false;
			}
			return true;
		} else
			return false;
	}

	public boolean isInGame() {
		return inGame;
	}

	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}

}
