package com.Fawaz.BrainSurvival.Screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class SplashScreen implements Screen {
	private BrainSurvival game;
	private Image splashPrimary, splashSecondary;
	private Stage stage;
	private boolean firstTime;
	private Preferences prefs = Gdx.app.getPreferences("Brain_Survival");

	public SplashScreen(BrainSurvival game) {
		this.game = game;
		firstTime = prefs.getBoolean("firstTime", true);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {
		splashPrimary = new Image(game.getResourceFactory().getSplashPrimary());
		splashPrimary.setPosition(60, 220);
		splashPrimary.addAction(sequence(alpha(0), alpha(1, 2),
				run(new StartSecondary())));
		splashSecondary = new Image(game.getResourceFactory()
				.getSplashSecondary());
		splashSecondary.setPosition(115, 345);
		splashSecondary.addAction(alpha(0));
		stage = new Stage(640, 640, false);
		stage.addActor(splashPrimary);
		stage.addActor(splashSecondary);
		stage.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if (firstTime) {
					prefs.putBoolean("firstTime", false);
					prefs.flush();
					game.setScreen(new Tutorial(game));
				} else
					game.setScreen(new Main_Menu(game, false));
			}
		});
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
	}

	public class StartSecondary implements Runnable {

		@Override
		public void run() {
			splashSecondary.addAction(sequence(alpha(1, 1.5f),
					Actions.run(new Placement())));

		}

	}

	public class Placement implements Runnable {

		@Override
		public void run() {
			splashPrimary.addAction(sequence(moveTo(-70, 170, 2),
					Actions.run(new Mainmenu())));
			splashSecondary.addAction(moveTo(-10, 295, 2));
		}

	}

	public class Mainmenu implements Runnable {

		@Override
		public void run() {
			if (firstTime) {
				prefs.putBoolean("firstTime", false);
				prefs.flush();
				game.setScreen(new Tutorial(game));
			} else
				game.setScreen(new Main_Menu(game, false));
		}

	}

}