package com.Fawaz.BrainSurvival.Screens;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class Credit implements Screen {

	private Label label;
	private BrainSurvival game;
	private TextButton returntoMainMenu;
	private Stage stage;
	private ScrollPane sp;

	public Credit(BrainSurvival game) {
		this.game = game;

		TextButtonStyle tbs = new TextButtonStyle(null, game
				.getResourceFactory().getTRDfromBox(), null, game
				.getResourceFactory().getFont32());
		tbs.fontColor = Color.GREEN;
		returntoMainMenu = new TextButton("Return to main menu", tbs);
		returntoMainMenu.setBounds(1000 - returntoMainMenu.getPrefWidth(), 0,
				returntoMainMenu.getPrefWidth(),
				returntoMainMenu.getPrefHeight());
		stage = new Stage(1000, 550);
		Gdx.input.setInputProcessor(stage);
		returntoMainMenu.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				Credit.this.game.setScreen(new Main_Menu(Credit.this.game,
						false));
			}

		});
		LabelStyle ls = new LabelStyle(game.getResourceFactory().getDevFont(),
				Color.GREEN);
		String word = "This game was made by one Fawaz Joseph, but he couldn't do it alone"
				+ "\nThanks\n"
				+ "\nDavid Abimbola -- Being an Awesome friend"
				+ "\nChiamaka Okoroha -- Playtester "
				+ "\nCourtney Ramseur -- Playtester"
				+ "\nAlexia Crumpton -- Playtester"
				+ "\nManny -- Playtester"
				+ "\nJeff -- Playtester"
				+ "\n\nSpecial thanks"
				+ "\nLibgdx and the team behind it,Thanks for building the framework behind this game"
				+ "\nhttp://libgdx.badlogicgames.com/ "
				+ gcNiklas()
				+ "\n\nDr. Quincy Brown -- it was during research with her that I was inspired to look into android development"
				+ "\n\nThe guys at javagaming.org who helped with my numerous question."
				+ "\n\nTheInvader360 for helping with multiple screens"
				+ "\nhttp://theinvader360.blogspot.co.uk/ -- check out his blog and his awesome games"
				+ "\n\nGlenn Fiedler for helping with the multiplayer"
				+ "\nhttp://gafferongames.com/ -- check out his awesome tutorials"
				+ "\n\nDustin Riley -- for his excellent tutorials on libgdx"
				+ "\nhttp://www.youtube.com/user/doctoriley."
				+ "\n\nThis game \"Brain Survival\" uses these sounds from freesound:\n"
				+ "Tone hit by patchen ( http://www.freesound.org/people/patchen/ )"
				+ "\nwaterdrop24 by junggle (http://www.freesound.org/people/junggle/)"
				+ "\nMICRO_BELL by Snapper4298 ( http://freesound.org/people/Snapper4298/ )"
				+ "\nGrenadine-Pair-2 by Seidhepriest (http://www.freesound.org/people/Seidhepriest/)"
				+ "\n\nIcons created by Android Asset Studio is licensed under a Creative Commons Attribution 3.0 Unported License."
				+ "\nhttp://android-ui-utils.googlecode.com/hg/asset-studio/dist/index.html"
				+ "\n\n Let's go Digital font - Created in 2013 by WLM Fonts";
		label = new Label(word, ls);
		label.setWrap(true);
		Group group = new Group();
		group.setBounds(0, 0, 1000, 550);
		// label.setBounds(0, 0, 1000, 500);
		sp = new ScrollPane(label);
		sp.setBounds(0, 50, 1000, 500);
		group.addActor(sp);
		group.addActor(returntoMainMenu);
		stage.addActor(group);
		// stage.addActor(returntoMainMenu);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

	public String gcNiklas() {
		if (Gdx.app.getType() != ApplicationType.iOS)
			return "";
		else
			return "\niOS version made possible by Niklas Therning and theRobovm Team -- robovm.org"
					+ "\nAdColony custom bindings by HD_92 on badlogic forums"
					+ "\nGame Center custom bindings by HD_92 and YaW on badlogic forums";
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
