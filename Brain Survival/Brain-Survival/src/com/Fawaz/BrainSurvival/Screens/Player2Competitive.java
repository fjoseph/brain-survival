package com.Fawaz.BrainSurvival.Screens;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.Fawaz.BrainSurvival.Utils.GameMaster;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class Player2Competitive implements Screen {

	Stage stage;
	int size = 0;
	Array<Sprite> elipsis = new Array<Sprite>();
	GameMaster gm;

	public Player2Competitive(BrainSurvival game, int i) {
		stage = new Stage(1280, 680, false);
		Gdx.input.setInputProcessor(stage);
		gm = new GameMaster(game, stage, i);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		gm.render(delta);
		stage.act();
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {
		gm.login();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		gm.dispose();
		stage.dispose();

	}

}
