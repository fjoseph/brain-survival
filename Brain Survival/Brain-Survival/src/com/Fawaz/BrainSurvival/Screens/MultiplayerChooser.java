package com.Fawaz.BrainSurvival.Screens;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class MultiplayerChooser extends InputListener implements Screen {

	Stage stage = new Stage(640, 640, false);
	TextButton findRoom, makeRoom, return_MainMenu;
	BrainSurvival game;
	Label roomCode;
	int choice;

	public MultiplayerChooser(BrainSurvival game) {
		this.game = game;
		Gdx.input.setInputProcessor(stage);
		TextButtonStyle tbs = new TextButtonStyle(null, game
				.getResourceFactory().getTRDfromBox(), null, game
				.getResourceFactory().getFont12());
		tbs.fontColor = Color.GREEN;
		findRoom = new TextButton("Find or make a public room", tbs);
		makeRoom = new TextButton("Make a private room", tbs);
		// findPrivateRoom = new TextButton("Click to find , style)
		return_MainMenu = new TextButton("return to main menu", tbs);
		findRoom.setBounds(320 - (.5f * findRoom.getPrefWidth()),
				400 - (.5f * findRoom.getPrefHeight()),
				findRoom.getPrefWidth(), findRoom.getPrefHeight());
		makeRoom.setBounds(320 - (.5f * makeRoom.getPrefWidth()),
				320 - (.5f * makeRoom.getPrefHeight()),
				makeRoom.getPrefWidth(), makeRoom.getPrefHeight());

		findRoom.addListener(new ScreenChanger(1));
		return_MainMenu
				.setBounds(640 - return_MainMenu.getPrefWidth(), 0,
						return_MainMenu.getPrefWidth(),
						return_MainMenu.getPrefHeight());
		makeRoom.addListener(new ScreenChanger(2));
		return_MainMenu.addListener(new ScreenChanger(5));
		LabelStyle tfStyle = new LabelStyle(game.getResourceFactory()
				.getFont12(), Color.GREEN);
		roomCode = new Label("", tfStyle);
		roomCode.setText("Tap to enter private room ID");
		roomCode.setBounds(findRoom.getX(),
				240 - (.5f * roomCode.getPrefHeight()),
				roomCode.getPrefWidth(), roomCode.getPrefHeight());
		roomCode.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				notAndroidGetName();
				return true;
			}
		});
		stage.addActor(findRoom);
		stage.addActor(return_MainMenu);
		stage.addActor(roomCode);
		stage.addActor(makeRoom);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
	}

	public class ScreenChanger extends InputListener {

		private int s;

		public ScreenChanger(int s) {
			this.s = s;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			return true;
		}

		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			stage.dispose();
			switch (s) {
			case 1:
				game.setScreen(new Player2Competitive(game, 1));
				break;
			case 2:
				game.setScreen(new Player2Competitive(game, 2));
				break;
			case 5:
				game.setScreen(new Main_Menu(game, false));
				break;
			}

		}
	}

	public void notAndroidGetName() {
		Gdx.input.getTextInput(new TextInputListener() {

			@Override
			public void input(String text) {

				try {
					game.setScreen(new Player2Competitive(game, Integer
							.parseInt(text)));
				} catch (NumberFormatException e) {
					roomCode.setText("Enter a valid Room Id\n Try again");
				}
				stage.setKeyboardFocus(null);

			}

			@Override
			public void canceled() {
				stage.setKeyboardFocus(null);
			}
		}, "Room id", "");
	}

}
