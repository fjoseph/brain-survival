package com.Fawaz.BrainSurvival.Screens;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.Entity.Player.interFaces;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Time_Attack implements Screen {

	Stage stage;
	Player p;

	public Time_Attack(BrainSurvival game) {
		stage = new Stage(640, 680, false);
		Gdx.input.setInputProcessor(stage);
		p = new Player(0, 0, Color.GREEN, game.getResourceFactory(), null);
		stage.addActor(p);
		p.start(game);
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		p.render(delta);
		stage.act();
		stage.draw();
		if (p.getInterface() == interFaces.PHYSICS)
			p.setStep();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
