package com.Fawaz.BrainSurvival.Screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;

public class Main_Menu implements Screen {

	private BrainSurvival game;
	private TextButton timeAttack, tutorial, multiPlayer, credits;
	private String[] badWordList = { "anal", "anus", "arrse", "arse", "ass",
			"fuck", "assfukka", "balls", "bastard", "beastial", "bellend",
			"bestial", "bestiality", "biatch", "bitch", "bloody", "blowjob",
			"boiolas", "bollock", "bollok", "boner", "boob", "booobs",
			"boooobs", "booooobs", "booooooobs", "breast", "buceta", "bugger",
			"bum", "butt", "cawk", "chink", "cipa", "cl1t", "clit", "cnut",
			"cock", "cok", "coon", "cox", "crap", "cum", "cunilingus",
			"cunillingus", "cunnilingus", "cunt", "cyalis", "damn", "dick",
			"dildo", "dink", "dirsa", "dlck", "doggin", "donkeyribber",
			"doosh", "duche", "dyke", "ejaculat", "ejakulate", "fag", "fanny",
			"fatass", "fcuk", "fecker", "felching", "fellate", "fellatio",
			"flange", "fook", "fudgepacker", "fuk", "fux", "fuxor", "gangbang",
			"goatse", "goddam", "sex", "heshe", "hoar", "hoare", "homo",
			"horniest", "horny", "jackoff", "jerkoff", "jism", "jiz", "jizm",
			"kawk", "knob", "kock", "kondum", "kum", "kunilingus", "lust",
			"masochist", "masterbate", "masterbation", "masturbate", "mofo",
			"mothafuck", "muff", "mutha", "muther", "mutherfucker", "nazi",
			"nigga", "nigger", "nob", "numbnuts", "nutsack", "orgasim",
			"orgasm", "pecker", "penis", "phonesex", "phuck", "phuk", "phuq",
			"pigfucker", "piss", "poop", "porn", "prick", "pron", "pube",
			"pusse", "pussi", "pussies", "pussy", "rectum", "retard", "rimjaw",
			"rimming", "shit", "sadist", "schlong", "scroat", "scrote",
			"scrotum", "semen", "shag", "shemale", "shit", "skank", "slut",
			"sluts", "smegma", "smut", "snatch", "sonofabitch", "spac",
			"spunk", "teets", "teez", "testical", "testicle", "tit", "tosser",
			"twat", "twunt", "twunter", "vagina", "vulva", "w00se", "wang",
			"wank", "whoar", "whore", "willies", "willy"

	};
	private Preferences prefs = Gdx.app.getPreferences("Brain_Survival");
	private Label nameLabel, won, lost, achievement, leaderboard;
	private Stage sStage;
	private Image splashPrimary, splashSecondary;
	private TextField textField;

	public Main_Menu(BrainSurvival g, boolean fromSplash) {
		this.game = g;
		// initialization
		sStage = new Stage(640, 640, false);
		splashPrimary = new Image(game.getResourceFactory().getSplashPrimary());
		splashPrimary.setPosition(-70, 170);
		splashSecondary = new Image(game.getResourceFactory()
				.getSplashSecondary());
		splashSecondary.setPosition(-10, 295);
		sStage.addActor(splashPrimary);
		sStage.addActor(splashSecondary);

		Gdx.input.setInputProcessor(sStage);
		TextButtonStyle buttonStyle = new TextButtonStyle();
		buttonStyle.font = game.getResourceFactory().getFont32();
		buttonStyle.fontColor = Color.GREEN;
		buttonStyle.pressedOffsetX = 40;
		// initialization

		// TextField
		TextFieldStyle tfStyle = new TextFieldStyle();
		tfStyle.font = game.getResourceFactory().getFont12();
		tfStyle.fontColor = Color.GREEN;
		tfStyle.messageFont = game.getResourceFactory().getFont12();
		;
		tfStyle.messageFontColor = Color.GREEN;
		tfStyle.cursor = game.getResourceFactory().getCursorDrawable();
		textField = new TextField("", tfStyle);
		textField.setPosition(490, 500);
		textField.setSize(230, textField.getHeight());
		textField.setMaxLength(12);
		initializePreference();
		if (Gdx.app.getType() == ApplicationType.Android) {
			textField.setTextFieldListener(new TextFieldListener() {

				@Override
				public void keyTyped(TextField textField, char key) {
					if (key == '\n') {
						textField.getOnscreenKeyboard().show(false);
						Main_Menu.this.sStage.setKeyboardFocus(null);
						String s = textField.getText();
						if (!s.isEmpty())
							if (!checkString(s)) {
								textField.setText("");
								textField.setMessageText("NO BAD WORDS");
							} else {
								prefs.putString("name", s.toUpperCase());
								prefs.flush();
							}

					}
				}
			});

			textField.setTextFieldFilter(new TextFieldFilter() {

				@Override
				public boolean acceptChar(TextField textField, char key) {
					switch (key) {
					case '!':
					case '@':
					case '#':
					case '$':
					case '%':
					case '^':
					case '&':
					case '*':
					case '(':
					case ')':
					case '_':
					case '-':
					case '+':
					case '=':
					case ':':
					case ';':
					case '"':
					case '~':
					case '`':
					case '<':
					case '>':
					case '.':
					case ',':
					case '/':
					case '?':
					case '\'':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case '0':
						return false;
					default:
						return true;
					}
				}
			});
		} else {
			textField.addListener(new InputListener() {
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					notAndroidGetName();
					return true;
				}

				public void touchUp(InputEvent event, float x, float y,
						int pointer, int button) {
				}
			});
		}
		// TextField

		// Labels
		LabelStyle lstyle = new LabelStyle(game.getResourceFactory()
				.getFont12(), Color.GREEN);
		nameLabel = new Label("NAME:", lstyle);
		won = new Label("WON: " + prefs.getInteger("Won"), lstyle);
		lost = new Label("LOST: " + prefs.getInteger("Lost"), lstyle);
		nameLabel.setPosition(435, 500);
		won.setPosition(435, 450);
		lost.setPosition(435, 400);
		achievement = new Label("achievements", lstyle);
		leaderboard = new Label("leaderboard", lstyle);

		achievement.setPosition(610 - achievement.getPrefWidth(), 40);
		leaderboard.setPosition(610 - leaderboard.getPrefWidth(), 90);

		achievement.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Main_Menu.this.game.displayAchievements();
				return true;
			}
		});
		leaderboard.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Main_Menu.this.game.displayLeaderBoards();
				return true;
			}
		});
		textField.setPosition(490, 500);
		textField.setSize(230, textField.getHeight());
		initializePreference();
		// Labels

		// Button Initialization
		timeAttack = new TextButton("Time Attack", buttonStyle);
		tutorial = new TextButton("TUTORIAL", buttonStyle);
		multiPlayer = new TextButton("MULTIPLAYER", buttonStyle);
		credits = new TextButton("CREDIT", buttonStyle);

		// Skip splashscreen
		if (fromSplash) {
			timeAttack.addAction(sequence(alpha(0), alpha(1, 1.5f)));
			tutorial.addAction(sequence(alpha(0), alpha(1, 1.5f)));
			multiPlayer.addAction(sequence(alpha(0), alpha(1, 1.5f)));
			credits.addAction(sequence(alpha(0), alpha(1, 1.5f)));
			won.addAction(sequence(alpha(0), alpha(1, 1.5f)));
			nameLabel.addAction(sequence(alpha(0), alpha(1, 1.5f)));
			lost.addAction(sequence(alpha(0), alpha(1, 1.5f)));
		}

		timeAttack.setPosition(0, 80);
		tutorial.setPosition(0, 40);
		multiPlayer.setPosition(0, 120);
		credits.setPosition(0, 0);

		timeAttack.addListener(new ScreenChanger(3));
		tutorial.addListener(new ScreenChanger(2));
		multiPlayer.addListener(new ScreenChanger(1));
		credits.addListener(new ScreenChanger(4));
		// Button Initialization

		// sStage
		sStage.addActor(nameLabel);
		sStage.addActor(won);
		sStage.addActor(lost);
		sStage.addActor(timeAttack);
		sStage.addActor(tutorial);
		sStage.addActor(multiPlayer);
		sStage.addActor(credits);
		sStage.addActor(textField);
		sStage.addActor(achievement);
		sStage.addActor(leaderboard);
		// sStage
		// sStage.setKeyboardFocus(textField);
		// textField.getOnscreenKeyboard().show(true);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		sStage.act();
		sStage.draw();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		sStage.dispose();

	}

	private boolean checkString(String word) {
		for (int i = 0; i < badWordList.length; i++) {
			if (word.toLowerCase().contains(badWordList[i]))
				return false;
		}
		return true;

	}

	private void initializePreference() {
		String name = prefs.getString("name", null);
		if (name == null)
			textField.setMessageText("TAP HERE");
		else
			textField.setText(name);
		int Won = prefs.getInteger("Won", -1);
		int Lost = prefs.getInteger("Lost", -1);
		if (Won == -1) {
			Won = 0;
			prefs.putInteger("Won", 0);
		}
		if (Lost == -1) {
			Lost = 0;
			prefs.putInteger("Lost", 0);
		}
		prefs.flush();
	}

	public void notAndroidGetName() {
		Gdx.input.getTextInput(new TextInputListener() {

			@Override
			public void input(String text) {
				char[] array = text.toCharArray();
				for (char c : array) {
					if (!Character.isLetter(c)) {
						textField.setText("No symbols");
						sStage.setKeyboardFocus(null);
						return;
					}
				}
				if (!text.isEmpty())
					if (!checkString(text)) {
						textField.setText("NO BAD WORDS");
						sStage.setKeyboardFocus(null);
					} else if (text.length() > 12) {
						textField.setText("12 letter limit");
					} else {
						prefs.putString("name", text.toUpperCase());
						textField.setText(text);
						prefs.flush();
						sStage.setKeyboardFocus(null);
					}

			}

			@Override
			public void canceled() {
				sStage.setKeyboardFocus(null);
			}
		}, "Name", "");
	}

	public class ScreenChanger extends InputListener {

		private int s;

		public ScreenChanger(int s) {
			this.s = s;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			return true;
		}

		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			sStage.dispose();
			switch (s) {
			case 1:
				game.setScreen(new MultiplayerChooser(game));
				break;
			case 2:
				game.setScreen(new Tutorial(game));
				break;
			case 3:
				game.unlockAchievements(0);
				game.setScreen(new Time_Attack(game));
				break;
			case 4:
				game.setScreen(new Credit(game));
				break;
			}

		}
	}

}