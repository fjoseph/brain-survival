package com.Fawaz.BrainSurvival.Screens;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class Tutorial implements Screen {

	private Stage stage;
	private ResourceFactory r;
	private Image screenShot;
	private Label explanation;
	private String[] explanationText;
	private TextButton back, next, returnMainMenu;
	private int index = -1;

	public Tutorial(final BrainSurvival game) {
		stage = new Stage(640, 640);
		explanationText = new String[12];
		screenShot = new Image();
		r = game.getResourceFactory();
		TextButtonStyle tbs = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		tbs.fontColor = Color.GREEN;
		back = new TextButton("back", tbs);
		next = new TextButton("next", tbs);
		returnMainMenu = new TextButton("main menu", tbs);
		explanation = new Label("", new LabelStyle(r.getFont12(), Color.GREEN));
		explanationText[0] = "Brain Survival is a game about simple puzzles complicated by adding the human element."
				+ "\nThere are 4 puzzles so far"
				+ "\nThe four puzzles are"
				+ "\n     + Missing Numbers"
				+ "\n          - Get from one number to another using arithmetic operation"
				+ "\n     +Musical Simon Says"
				+ "\n          - Reproduce an order of musical notes"
				+ "\n     +Color Matching"
				+ "\n          - Hit balls when they match the given color"
				+ "\n     +Shell Game"
				+ "\n          -  Guess where the small circle is after it has shuffled"
				+ "\n\nMultiplayer "
				+ "\n+2 players competitive"
				+ "\n  - Solve puzzles to add time on a decreasing timer while sabotaging your opponent's game"
				+ "\nSinglePlayer"
				+ "\n+Time Attack"
				+ "\nTry to have the longest time in a global leaderboard"
				+ "\n-Solve puzzles to add time on a decreasing timer to have the highest "
				+ "\n\n This tutorial is highly recommended for first timers";
		explanationText[1] = "This is called Missing numbers"
				+ "\n The objective of this minigame is to get from the number"
				+ " of the far left to the number on the far right using one of the operation below"
				+ "\n Let's take this instance for an example"
				+ "\n\nWe have 14 and we have to get to 80"
				+ "\n The answer out of all three of them is +66";
		explanationText[2] = "This is a simple game of color matching"
				+ "\n when the color of a ball matches the text, press the ball";
		explanationText[3] = "Every ball matched correctly gives you some time";
		explanationText[4] = "This is a simple variation of simon says"
				+ "\n watch the order of notes played, Replicate it";
		explanationText[5] = "This is called \"The shell game\""
				+ "\n The small circle [read: shell] goes to the one of the bigger circles"
				+ "\nand then the bigger circles randomly shuffles around. Pick out the location of the small circle"
				+ "\n Each correctly matched note gives you some time";
		explanationText[6] = "The main atttraction of this game is the multiplayer"
				+ "\n Each player starts with  30 seconds on a timer that is counting down"
				+ "\nThe first person to run out of time loses"
				+ "\nEvery minigame you win gains you time"
				+ "\n Inversely you lose time when you lose a minigame"
				+ "\nThere are however ways you can \"help\" your opponent lose their time";
		explanationText[7] = "Let's Say you are the player on the right and you have three points"
				+ "\n You can one of those points to hit blind on your opponent's game";
		explanationText[8] = "An obnoxious smiley of your color obstructs the view of your opponent"
				+ " preventing them from gaining time";
		explanationText[9] = "With two points you can actively influence your opponent's game"
				+ "\n Your opponent's choice on the left that will give him time and points is +19";
		explanationText[10] = "If you use your two points to pick any other choice than +19"
				+ "\n You can actually use your points to lose their time for them"
				+ "\n\n if an opponent influences your game, then your timer will flash their color";
		explanationText[11] = "Good Luck" + "\n May the smartest person win";

		back.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				transition(false);
			}
		});

		next.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				transition(true);
			}
		});

		returnMainMenu.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.setScreen(new Main_Menu(game, false));
			}
		});
		stage.addActor(back);
		stage.addActor(next);
		stage.addActor(returnMainMenu);
		stage.addActor(screenShot);
		stage.addActor(explanation);
		explanation.setWrap(true);
		Gdx.input.setInputProcessor(stage);
		transition(true);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();

	}

	public void transition(boolean direction) {
		if (direction)
			index++;
		else
			index--;

		explanation.setText(explanationText[index]);
		screenShot.setDrawable(r.getTutorialDrawable(index));

		switch (index) {
		case 0:
			back.setVisible(false);
			next.setBounds(640 - next.getPrefWidth(), 0, next.getPrefWidth(),
					next.getPrefHeight());
			returnMainMenu.setBounds(0, 0, returnMainMenu.getPrefWidth(),
					returnMainMenu.getPrefHeight());
			explanation.setBounds(0, back.getPrefHeight(), 640,
					640 - back.getPrefHeight());
			break;
		case 11:
			back.setVisible(true);
			next.setVisible(false);
			returnMainMenu.setBounds(640 - returnMainMenu.getPrefWidth(), 0,
					returnMainMenu.getPrefWidth(),
					returnMainMenu.getPrefHeight());
			back.setBounds(0, 0, back.getPrefWidth(), back.getPrefHeight());
			explanation.setBounds(0, back.getPrefHeight(), 640,
					640 - back.getPrefHeight());
			break;
		case 6:
			back.setVisible(true);
			next.setVisible(true);
			back.setBounds(0, 0, back.getPrefWidth(), back.getPrefHeight());
			next.setBounds(640 - next.getPrefWidth(), 0, next.getPrefWidth(),
					next.getPrefHeight());
			returnMainMenu.setBounds(
					320 - (.5f * returnMainMenu.getPrefWidth()), 0,
					returnMainMenu.getPrefWidth(),
					returnMainMenu.getPrefHeight());
			explanation.setBounds(0, back.getPrefHeight(), 640,
					640 - back.getPrefHeight());
			break;
		default:
			back.setVisible(true);
			next.setVisible(true);
			back.setBounds(0, 0, back.getPrefWidth(), back.getPrefHeight());
			next.setBounds(640 - next.getPrefWidth(), 0, next.getPrefWidth(),
					next.getPrefHeight());
			returnMainMenu.setBounds(
					320 - (.5f * returnMainMenu.getPrefWidth()), 0,
					returnMainMenu.getPrefWidth(),
					returnMainMenu.getPrefHeight());
			screenShot.setBounds(0, back.getPrefHeight(), 320,
					640 - back.getPrefHeight());
			explanation.setBounds(320, back.getPrefHeight(), 320,
					640 - back.getPrefHeight());
			break;
		}

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();

	}

}