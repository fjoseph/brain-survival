package com.Fawaz.BrainSurvival.Net.PuppetGroups;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Memory.Model.MusicImage;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.Fawaz.BrainSurvival.Utils.Demo;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class PuppetSimonSays extends MicroGame implements Demo {

	private Array<Sound> soundFiles;
	private Array<Color> colorArray;
	private Array<MusicImage> musicHolder;
	private KryoClient client;
	private Player p;
	private int k;

	public PuppetSimonSays(Player p, ResourceFactory r) {
		setBounds(0, 40, 640, 640);
		client = p.getClient();
		this.p = p;
		soundFiles = r.getSoundFiles();
		colorArray = new Array<Color>();
		musicHolder = new Array<MusicImage>();
		colorArray.add(Color.GREEN);
		colorArray.add(Color.BLUE);
		colorArray.add(Color.RED);
		colorArray.add(Color.YELLOW);
		TextButton blindOpponent;
		TextButtonStyle bstyle = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		bstyle.fontColor = p.getPlayerColor();
		blindOpponent = new TextButton("Blind", bstyle);
		blindOpponent.setBounds(
				(float) (270 - (.5 * blindOpponent.getPrefWidth())), 350,
				blindOpponent.getPrefWidth(), blindOpponent.getPrefHeight());
		blindOpponent.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if (PuppetSimonSays.this.p.enoughPoints(1))
					client.sendBlind();
			}
		});
		addActor(blindOpponent);
		CreateWorld(r);
	}

	private void CreateWorld(ResourceFactory r) {

		int i = 50;
		int j = 0;
		TextureRegionDrawable ud;
		for (j = 0; j < 4; j++) {
			ud = r.getTRDfromCircle();
			MusicImage mb = new MusicImage(ud);
			if (j == 1 || j == 3)
				mb.setBounds(i, 100, 100, 100);
			else
				mb.setBounds(i, 420, 100, 100);

			mb.setName(Integer.toString(j));
			mb.addListener(new MusicalListener(mb));
			i = i + 155;
			musicHolder.add(mb);
			addActor(mb);
		}

		i = 0;
		for (MusicImage m : musicHolder) {
			m.setColor(colorArray.get(i));
			m.setSound(soundFiles.get(i));
			i++;
		}
	}

	@Override
	public void init() {
		k = 0;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	public void setSimonOrder(IntArray iA) {
		float j = 0;
		for (int i = 0; i < 5; i++) {
			musicHolder.get(iA.get(i));
			j += .5;
			Timer.schedule(new Click(musicHolder.get(iA.get(i))), j);
			j += .5;
			Timer.schedule(new UnClick(musicHolder.get(iA.get(i))), j);
		}

	}

	public class Click extends Task {
		MusicImage mi;

		public Click(MusicImage mi) {
			this.mi = mi;
		}

		@Override
		public void run() {
			mi.click(false);
		}

	}

	public class UnClick extends Task {
		MusicImage mi;

		public UnClick(MusicImage mi) {
			this.mi = mi;
		}

		@Override
		public void run() {
			mi.unClick();
			k++;
		}

	}

	@Override
	public void handleButtonPress(int index) {
		Timer.schedule(new Click(musicHolder.get(index)), (float) .3);
		Timer.schedule(new UnClick(musicHolder.get(index)), (float) .7);
	}

	public class MusicalListener extends InputListener {
		private MusicImage mb;

		public MusicalListener(MusicImage mb) {
			this.mb = mb;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {

			if (p.enoughPoints(2)) {
				if (client != null)
					client.sendInfluence(2, musicHolder.indexOf(mb, false));
				mb.click(true);
			}
			return true;
		}

		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			mb.unClick();
		}

	}

	@Override
	public boolean render(float delta) {
		return k == 5;
	}

}
