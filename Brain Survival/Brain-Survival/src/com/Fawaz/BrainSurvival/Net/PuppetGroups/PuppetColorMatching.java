package com.Fawaz.BrainSurvival.Net.PuppetGroups;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.after;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.color;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Reaction.Model.ColorMatchingGroup.ProbabilityMachine;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.ColorMatchingImagePacket;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;

public class PuppetColorMatching extends MicroGame {
	private Array<Image> imageHolder;
	private Label ColorRingGirl;
	private Pool<ImageMoveMent> movementPool;

	public PuppetColorMatching(final Player p, ResourceFactory r) {
		setBounds(0, 40, 640, 640);
		LabelStyle ColorRingGirlStyle = new LabelStyle();
		imageHolder = new Array<Image>(3);
		BitmapFont font = r.getFont32();
		ColorRingGirlStyle.font = font;
		ColorRingGirl = new Label("", ColorRingGirlStyle);
		ColorRingGirl.setPosition(0, 520);
		ColorRingGirl.setWidth(640);
		ColorRingGirl.setAlignment(Align.center);
		ColorRingGirl.setFontScale(1.5f);
		addActor(ColorRingGirl);
		TextButton blindOpponent;
		TextButtonStyle bstyle = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		bstyle.fontColor = p.getPlayerColor();
		blindOpponent = new TextButton("Blind", bstyle);
		blindOpponent.setBounds(
				(float) (320 - (.5 * blindOpponent.getPrefWidth())), 350,
				blindOpponent.getPrefWidth(), blindOpponent.getPrefHeight());
		blindOpponent.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if (p.enoughPoints(1))
					p.getClient().sendBlind();
			}
		});
		addActor(blindOpponent);
		Image image;
		for (int i = 0; i < 3; i++) {
			image = new Image();
			image.setDrawable(r.getTRDfromCircle());
			imageHolder.add(image);
			addActor(image);
			image.setBounds(112 + 160 * i, 464, 96, 96);
			image.addListener(new buttonListener(image, p));
		}

		movementPool = new Pool<ImageMoveMent>() {

			@Override
			protected ImageMoveMent newObject() {
				return new ImageMoveMent();
			}

		};

	}

	public class buttonListener extends InputListener {
		Image I;
		Player p;

		public buttonListener(Image I, Player player) {
			this.I = I;
			p = player;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			if (p.enoughPoints(2)) {
				p.getClient().sendInfluence(1, imageHolder.indexOf(I, false));
			}
			return true;
		}

	}

	@Override
	public void init() {
		for (Image i : imageHolder)
			i.setVisible(true);
	}

	@Override
	public void reset() {
	}

	public void setImageStatus(ColorMatchingImagePacket[] image) {
		ImageMoveMent im = movementPool.obtain();
		im.init(image);
		Gdx.app.postRunnable(im);
	}

	public void changeLabelColor(int labelColor) {
		switch (labelColor) {
		case 1:
			ColorRingGirl.setText("Green".toUpperCase());
			ColorRingGirl.setColor(Color.GREEN);
			break;
		case 2:
			ColorRingGirl.setText("Red".toUpperCase());
			ColorRingGirl.setColor(Color.RED);
			break;
		case 3:
			ColorRingGirl.setText("Yellow".toUpperCase());
			ColorRingGirl.setColor(Color.YELLOW);
			break;
		case 4:
			ColorRingGirl.setText("Blue".toUpperCase());
			ColorRingGirl.setColor(Color.BLUE);
			break;
		}
	}

	@Override
	public void handleButtonPress(int index) {
	}

	private class ImageMoveMent implements Runnable, Poolable {

		ColorMatchingImagePacket[] image;

		public void init(ColorMatchingImagePacket[] image) {
			this.image = image;
		}

		@Override
		public void run() {

			int i = 0;
			for (ColorMatchingImagePacket iInf : image) {
				Color c = null;
				for (ProbabilityMachine p : ProbabilityMachine.values()) {
					if (iInf.colorCode == p.getCode()) {
						c = p.getColor();
						break;
					}
				}

				imageHolder.get(i++)
						.addAction(
								after(sequence(
										color(c),
										moveTo(iInf.x, iInf.y, .06f,
												Interpolation.sine))));
			}

			i = 2;
			for (int k = 0; k < (3 - image.length); k++, i--) {
				imageHolder.get(i).setVisible(false);
			}

			movementPool.free(this);

		}

		@Override
		public void reset() {
			image = null;

		}

	}

}