package com.Fawaz.BrainSurvival.Net.PuppetGroups;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.after;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.Iterator;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.Utils.Demo;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;

public class PuppetCupGame extends MicroGame implements Demo {

	private int count;

	private enum Positions {
		FIRST(120f), SECOND(270f), THIRD(420f);

		float x;
		float y = 140;

		Positions(float x) {
			this.x = x;
		}

		public float getX() {
			return x;
		}

		public float getY() {
			return y;
		}
	}

	private Image image;
	private Array<Color> colorArray;
	private Array<Image> cupArray;
	private Player p;
	private int Neo;
	
	public PuppetCupGame(Color c, Player player, ResourceFactory r) {
		p = player;
		setBounds(0, 40, 640, 640);
		cupArray = new Array<Image>();
		colorArray = new Array<Color>();
		colorArray.add(Color.GREEN);
		colorArray.add(Color.BLUE);
		colorArray.add(Color.YELLOW);
		TextButton blindOpponent;
		TextButtonStyle bstyle = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		bstyle.fontColor = p.getPlayerColor();
		blindOpponent = new TextButton("Blind", bstyle);
		blindOpponent.setBounds(
				(float) (320 - (.5 * blindOpponent.getPrefWidth())), 350,
				blindOpponent.getPrefWidth(), blindOpponent.getPrefHeight());
		blindOpponent.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if (PuppetCupGame.this.p.enoughPoints(1))
					PuppetCupGame.this.p.getClient().sendBlind();
			}
		});
		addActor(blindOpponent);
		for (int i = 0; i < 3; i++) {
			image = new Image(r.getTRDfromCircle());
			addActor(image);
			image.setName("");
			cupArray.add(image);
		}

		image = new Image(r.getTRDfromCircle());
		image.setColor(c);
		addActor(image);
		image.setColor(p.getPlayerColor());
	}

	@Override
	public void init() {
		count = 0;
		Iterator<Image> ImIter = cupArray.iterator();
		Iterator<Color> iC = colorArray.iterator();
		Image i;
		for (Positions p : Positions.values()) {
			i = ImIter.next();
			i.addAction(moveTo(p.getX(), p.getY()));
			i.setColor(iC.next());
			i.setSize(100, 100);
			i.addListener(new CupListener(i, this.p));
		}
		image.setBounds(Positions.SECOND.getX() + 30, 410, 50, 50);
	}

	@Override
	public void reset() {
		for (Image i : cupArray) {
			i.removeListener(i.getListeners().first());
			i.setVisible(true);
		}
	}

	public void switchCups(int First, int Second) {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Image f, s;
		f = cupArray.get(First);
		s = cupArray.get(Second);
		f.addAction(after(moveTo(s.getX(), s.getY(), .2f)));
		s.addAction(after(moveTo(f.getX(), f.getY(), .2f)));
		f.setColor(colorArray.random());
		s.setColor(colorArray.random());
		count++;
	}

	private void setNeo(int index) {
		float neoX = 0, neoY = 0;
		switch (index) {
		case 0:
			neoX = Positions.FIRST.getX() + 25;
			neoY = Positions.FIRST.getY() + 25;
			break;
		case 1:
			neoX = Positions.SECOND.getX() + 25;
			neoY = Positions.SECOND.getY() + 25;
			break;
		case 2:
			neoX = Positions.THIRD.getX() + 25;
			neoY = Positions.THIRD.getY() + 25;
			break;
		}
		image.addAction(sequence(moveTo(neoX, neoY, .5f), alpha(0)));
		image.toBack();
		Neo = index;

	}

	@Override
	public void handleButtonPress(int index) {
		if (index % 10 == 3) {
			setNeo(index / 10);
		} else {
			float neoX = 0, neoY = 0;
			neoX = cupArray.get(Neo).getX() + 25;
			neoY = cupArray.get(Neo).getY() + 25;
			image.setPosition(neoX, neoY);
			for (Image i : cupArray) {
				i.addAction(moveTo(i.getX(), 400, .5f));
			}
			image.addAction(alpha(1));
		}
	}

	private class CupListener extends InputListener {
		private Image i;
		private Color clr;
		private Player p;

		public CupListener(Image i, Player p) {
			this.i = i;
			clr = new Color(i.getColor());
			this.p = p;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {

			if (p.enoughPoints(2)) {
				for (Image i : cupArray) {
					i.addAction(moveTo(i.getX(), 400, .5f));
				}
				i.setColor(Color.WHITE);
				image.addAction(alpha(1));
				p.getClient().sendInfluence(3, cupArray.indexOf(i, false));
			}
			return true;
		}

		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			i.setColor(clr);
		}
	}

	@Override
	public boolean render(float delta) {
		return count == 7;
	}

}
