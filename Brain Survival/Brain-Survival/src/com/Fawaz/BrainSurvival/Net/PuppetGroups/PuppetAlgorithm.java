package com.Fawaz.BrainSurvival.Net.PuppetGroups;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.MiddleButtonPacket;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class PuppetAlgorithm extends MicroGame {

	private Array<TextButton> buttons;
	private Player p;
	private KryoClient client;

	public PuppetAlgorithm(Color c, Player p, ResourceFactory r) {
		client = p.getClient();
		setBounds(0, 40, 640, 640);
		buttons = new Array<TextButton>(7);
		TextButtonStyle normal = new TextButtonStyle(null, null, null,
				r.getFont32());
		TextButtonStyle pressable = new TextButtonStyle(null,
				r.getTRDfromBox(), r.getTRDfromBox(), r.getFont32());
		normal.fontColor = p.getPlayerColor();
		pressable.fontColor = p.getPlayerColor();
		this.p = p;
		for (int i = 0; i < 7; i++) {
			if (i <= 3) {
				buttons.add(new TextButton("", normal));
				buttons.peek().setBounds(i * 160, 400, 160, 64);
				buttons.peek().setOrigin(buttons.peek().getWidth() / 2,
						buttons.peek().getHeight() / 2);
			} else {
				buttons.add(new TextButton("", pressable));
				buttons.peek().setBounds((i - 4) * 213, 128, 213, 64);
				buttons.peek().setOrigin(buttons.peek().getWidth() / 2,
						buttons.peek().getHeight() / 2);
				buttons.peek().addListener(new ButtonClick(i));
			}
			addActor(buttons.peek());
		}

		buttons.get(1).setText("?");
		buttons.get(2).setText("=");

		TextButton blindOpponent;
		TextButtonStyle bstyle = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		bstyle.fontColor = p.getPlayerColor();
		blindOpponent = new TextButton("Blind", bstyle);
		blindOpponent.setBounds(
				(float) (320 - (.5 * blindOpponent.getPrefWidth())), 450,
				blindOpponent.getPrefWidth(), blindOpponent.getPrefHeight());
		blindOpponent.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if (PuppetAlgorithm.this.p.enoughPoints(1))
					client.sendBlind();
			}
		});
		addActor(blindOpponent);

	}

	public void changeMiddleButton(MiddleButtonPacket mbp) {
		buttons.get(0).setText(mbp.firstNumber);
		buttons.get(3).setText(mbp.secondNumber);
		buttons.get(4).setText(mbp.option1);
		buttons.get(5).setText(mbp.option2);
		buttons.get(6).setText(mbp.option3);

	}

	@Override
	public void handleButtonPress(final int index) {
		buttons.get(index).setChecked(true);

		Timer.schedule(new Task() {

			@Override
			public void run() {
				buttons.get(index).setChecked(false);

			}
		}, .3f);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	private class ButtonClick extends InputListener {
		int index;

		public ButtonClick(int index) {
			this.index = index;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			return true;
		}

		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			buttons.get(index).setChecked(false);
			if (p.enoughPoints(2))
				client.sendInfluence(0, index);
		}
	}

}
