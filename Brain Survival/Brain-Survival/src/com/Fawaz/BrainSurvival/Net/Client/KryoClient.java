package com.Fawaz.BrainSurvival.Net.Client;

import java.io.IOException;

import com.Fawaz.BrainSurvival.MicroGames.Reaction.Model.ColorMatchingGroup.ProbabilityMachine;
import com.Fawaz.BrainSurvival.Net.Client.Packets.DiscPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.LoginPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.LoginResponsePacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.PlayerInformationPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.PrivateRoomInformation;
import com.Fawaz.BrainSurvival.Net.Client.Packets.RoomRequestPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.StartPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.BlindPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.ButtonPressPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.ChangeGamePacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.ColorMatchingImagePacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.CupGameStatusPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.GameOverPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.InfluencePacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.LabelColor;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.MiddleButtonPacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.SimonSaysPacket;
import com.Fawaz.BrainSurvival.Utils.GameMaster;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;

public class KryoClient extends Listener {

	private Client client;
	private int version = 3;
	private Preferences prefs = Gdx.app.getPreferences("Brain_Survival");
	private GameMaster gm;
	private PlayerInformationPacket pi;
	private int choice;

	public KryoClient(GameMaster gameMaster, int choice) {
		gm = gameMaster;
		this.choice = choice;
		Gdx.app.setLogLevel(Log.LEVEL_DEBUG);
		client = new Client();
		pi = new PlayerInformationPacket();
		client.getKryo().register(ButtonPressPacket.class);
		client.getKryo().register(ChangeGamePacket.class);
		client.getKryo().register(ColorMatchingImagePacket.class);
		client.getKryo().register(ColorMatchingImagePacket[].class);
		client.getKryo().register(CupGameStatusPacket.class);
		client.getKryo().register(InfluencePacket.class);
		client.getKryo().register(GameOverPacket.class);
		client.getKryo().register(MiddleButtonPacket.class);
		client.getKryo().register(SimonSaysPacket.class);
		client.getKryo().register(int[].class);
		client.getKryo().register(LoginPacket.class);
		client.getKryo().register(LoginResponsePacket.class);
		client.getKryo().register(RoomRequestPacket.class);
		client.getKryo().register(StartPacket.class);
		client.getKryo().register(PlayerInformationPacket.class);
		client.getKryo().register(PlayerInformationPacket[].class);
		client.getKryo().register(DiscPacket.class);
		client.getKryo().register(LabelColor.class);
		client.getKryo().register(BlindPacket.class);
		client.getKryo().register(PrivateRoomInformation.class);

		client.addListener(this);

		client.start();

	}

	public void Login() {
		try {
			//client.connect(2000, "192.168.8.107", 55555, 55555);
			 client.connect(2000, "23.239.17.61", 55555,55555);
		} catch (IOException e) {
			gm.handleDisconnect(false);
			e.printStackTrace();
		}
		LoginPacket lm = new LoginPacket();
		lm.version = version;
		client.sendTCP(lm);

	}

	@Override
	public void connected(Connection connection) {
		connection.setKeepAliveTCP(2000);
		connection.setTimeout(5000);
	}

	@Override
	public void received(Connection connection, Object object) {
		Gdx.app.log("Received", object.getClass().getSimpleName());
		if (object instanceof LoginResponsePacket) {
			Gdx.app.debug("Received", "LoginResponse");
			LoginResponsePacket lrp = (LoginResponsePacket) object;
			if (lrp.accepted) {
				Gdx.app.debug("Login", "Processing Login Accepted");
				int Won = prefs.getInteger("Won", 0);
				int Lost = prefs.getInteger("Lost", 0);
				String name = prefs.getString("name", "NULL");
				RoomRequestPacket rrp = new RoomRequestPacket();
				rrp.name = name;
				rrp.won = Won;
				rrp.lost = Lost;
				rrp.RoomType = choice;
				client.sendTCP(rrp);
			} else {
				gm.handleLoginReject(lrp.Message);
			}
		} else if (object instanceof PlayerInformationPacket[]) {
			PlayerInformationPacket[] piArray = (PlayerInformationPacket[]) object;

			Gdx.app.debug("Received", "RoomInformation");
			gm.initializeLobby();
			int i = 0;
			for (PlayerInformationPacket pip : piArray) {
				if (pip.Id == connection.getID()) {
					pi.color = pip.color;
					pi.ready = pip.ready;
					gm.initializePlayer(i, piArray.length == 1, pip.color);
				} else {
					gm.initializeOpponent(i, pip.username, pip.won, pip.lost,
							pip.color, pip.ready);
				}
				i++;
			}

		} else if (object instanceof StartPacket) {
			StartPacket sp = (StartPacket) object;
			if (sp.start) {
				gm.initializeCountdown(sp.estimatedTime);
			} else
				gm.stopCountdown();
		} else if (object instanceof DiscPacket) {
			gm.handleDisconnect(true);
		} else if (object instanceof ChangeGamePacket) {
			Gdx.app.debug("Received", "ChangeGamePacket");
			ChangeGamePacket cgp = (ChangeGamePacket) object;
			gm.changeOpponentGame(cgp.index, cgp.points, cgp.elapsedTime);

		} else if (object instanceof MiddleButtonPacket) {
			MiddleButtonPacket mbp = (MiddleButtonPacket) object;
			gm.changeMiddleButton(mbp);
		} else if (object instanceof ButtonPressPacket) {
			Gdx.app.debug("Received", "Button Press");
			ButtonPressPacket bpp = (ButtonPressPacket) object;
			gm.handleButtonPress(bpp.index, bpp.button);
		} else if (object instanceof ColorMatchingImagePacket[]) {
			ColorMatchingImagePacket[] image = (ColorMatchingImagePacket[]) object;
			Gdx.app.debug("Color Matching Images", "Size of the array: "
					+ image.length);
			gm.setimageMovement(image);
		} else if (object instanceof LabelColor) {
			LabelColor lc = (LabelColor) object;
			gm.changeLabelColor(lc.color);
		} else if (object instanceof SimonSaysPacket) {
			Gdx.app.debug("Received", "SimonSaysPacket");
			SimonSaysPacket ssp = (SimonSaysPacket) object;
			IntArray simonOrder = new IntArray(5);
			simonOrder.addAll(ssp.items, 0, ssp.size);
			gm.setSimonOrder(simonOrder);
		} else if (object instanceof CupGameStatusPacket) {
			CupGameStatusPacket cgsp = (CupGameStatusPacket) object;
			gm.setCupStatus(cgsp.First, cgsp.Second);
		} else if (object instanceof BlindPacket) {
			gm.handleBlind(true);
		} else if (object instanceof InfluencePacket) {
			InfluencePacket ip = (InfluencePacket) object;
			gm.handeInfluence(ip.index, ip.buttonIndex);
		} else if (object instanceof GameOverPacket) {
			gm.handleGameOver();
			client.close();
		} else if (object instanceof PrivateRoomInformation) {
			PrivateRoomInformation pri = (PrivateRoomInformation) object;
			gm.setRoomID(pri.id);
		}
	}

	@Override
	public void disconnected(Connection connection) {
	}

	public void sendColor(int color) {
		pi.color = color;
		client.sendTCP(pi);
	}

	public void sendLabelColor(int choice) {
		LabelColor ccmp = new LabelColor();
		ccmp.color = choice;
		client.sendTCP(ccmp);
	}

	public void sendReady(boolean ready) {
		pi.ready = ready;
		client.sendTCP(pi);
	}

	public void endConnection() {
		client.close();

	}

	public void sendChangeGame(int indexOf, int points, float elapsedTime) {
		ChangeGamePacket cgp = new ChangeGamePacket();
		cgp.index = indexOf;
		cgp.points = points;
		cgp.elapsedTime = elapsedTime;
		Gdx.app.log("Sending", "ChangeGamePacket");
		client.sendTCP(cgp);

	}

	public void sendMiddleChange(MiddleButtonPacket mbp) {
		client.sendTCP(mbp);
	}

	public void sendButtonPress(int gameIndex, int buttonIndex) {
		ButtonPressPacket bpp = new ButtonPressPacket();
		bpp.index = gameIndex;
		bpp.button = buttonIndex;
		client.sendTCP(bpp);

	}

	public void sendImageStatus(Array<Image> imageHolder) {
		ColorMatchingImagePacket[] imageInformation = new ColorMatchingImagePacket[imageHolder.size];
		int i = 0;
		for (Image image : imageHolder) {
			imageInformation[i] = new ColorMatchingImagePacket();
			imageInformation[i].x = image.getX();
			imageInformation[i].y = image.getY();
			for (ProbabilityMachine p : ProbabilityMachine.values()) {
				if (image.getColor().equals(p.getColor())) {
					imageInformation[i++].colorCode = p.getCode();
					Gdx.app.debug("CupStatus",
							"The color code is: " + p.getCode());
					break;
				}
			}
		}

		client.sendUDP(imageInformation);

	}

	public void sendSimonOrder(IntArray order) {
		Gdx.app.debug("Simon Says", "Sending Simon Says Order");
		SimonSaysPacket sp = new SimonSaysPacket(5);
		sp.addAll(order.toArray());
		client.sendTCP(sp);
	}

	public void sendCupStatus(int first, int second) {
		CupGameStatusPacket cgsp = new CupGameStatusPacket();
		cgsp.First = first;
		cgsp.Second = second;
		client.sendTCP(cgsp);
	}

	public void sendInfluence(int gameCode, int buttonIndex) {

		InfluencePacket ip = new InfluencePacket();
		ip.index = gameCode;
		ip.buttonIndex = buttonIndex;
		client.sendTCP(ip);
	}

	public void sendBlind() {
		gm.handleBlind(false);
		BlindPacket bp = new BlindPacket();
		client.sendTCP(bp);
	}

	public void sendGameOver() {
		GameOverPacket gop = new GameOverPacket();
		client.sendTCP(gop);
	}

}