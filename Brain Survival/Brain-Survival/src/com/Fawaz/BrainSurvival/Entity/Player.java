package com.Fawaz.BrainSurvival.Entity;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.color;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.text.DecimalFormat;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.Fawaz.BrainSurvival.MicroGames.Logic.Model.ArithmeticGroup;
import com.Fawaz.BrainSurvival.MicroGames.Memory.Model.SimonSaysGroup;
import com.Fawaz.BrainSurvival.MicroGames.Reaction.Model.ColorMatchingGroup;
import com.Fawaz.BrainSurvival.MicroGames.Visual.Model.CupGameGroup;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.Fawaz.BrainSurvival.Screens.Main_Menu;
import com.Fawaz.BrainSurvival.Utils.Demo;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.PhysicsMovable;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Pool.Poolable;

public class Player extends Group {
	private Image timerImage;
	private Array<Image> pointHolder;
	private float elapsedTime, percent, Fulltimer = 30, FullWidth = 500,
			allTime = 0;
	private FPSLogger logger = new FPSLogger();
	private MicroGame currentAdd;
	private Array<MicroGame> microGameArray;
	private interFaces currentInterface;
	private TextButton exit;
	private int points = 0;
	private Preferences prefs = Gdx.app.getPreferences("Brain_Survival");
	private Color color;
	private Label Won, Lost, Name, Place;
	private KryoClient client;
	private Pool<HandleButtonPress> influencePool;
	private DecimalFormat df;
	private BrainSurvival game;

	public enum interFaces {
		PHYSICS, DEMO, NONE, GAMEOVER;
	}

	public Player(float x, float y, Color c, ResourceFactory r, KryoClient netC) {
		df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		client = netC;
		this.color = c;
		setSize(640, 640);
		y += 40;
		pointHolder = new Array<Image>(3);
		Image s;
		for (int i = 0; i < 3; i++) {
			s = new Image(r.getTRDfromCircle());
			s.setColor(c);
			s.setBounds(600 - (i * 40), 0, 40, 40);
			pointHolder.add(s);
		}
		timerImage = new Image(r.getTRDfromBox());
		TextButtonStyle tbs = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		tbs.fontColor = c;

		exit = new TextButton("Exit", tbs);
		exit.setBounds(0, 0, exit.getLabel().getPrefWidth(), 40);
		microGameArray = new Array<MicroGame>();
		microGameArray.add(new ArithmeticGroup(this.color, this, r));
		microGameArray.add(new ColorMatchingGroup(x, y, this, r));
		microGameArray.add(new SimonSaysGroup(this, r));
		microGameArray.add(new CupGameGroup(this.color, this, r));
		timerImage.setPosition(75, 300);
		timerImage.setSize(FullWidth, 10f);
		timerImage.setColor(c);
		LabelStyle ls = new LabelStyle(r.getFont32(), c);
		LabelStyle ls32 = new LabelStyle(r.getFont32(), c);
		Place = new Label("", ls32);
		Place.setPosition((float) (300 - .5 * Place.getPrefWidth()),
				(float) (400 - .5 * Place.getPrefHeight()));
		Name = new Label(prefs.getString("name", "null"), ls);
		Name.setPosition((float) (300 - .5 * Name.getPrefWidth()),
				(float) (350 - .5 * Name.getPrefHeight()));
		Won = new Label("WON: " + prefs.getInteger("Won"), ls);
		Won.setPosition((float) (300 - .5 * Won.getPrefWidth()),
				(float) (305 - .5 * Won.getPrefHeight()));
		Lost = new Label("LOST: " + prefs.getInteger("Lost"), ls);
		Lost.setPosition((float) (300 - .5 * Lost.getPrefWidth()),
				(float) (255 - .5 * Lost.getPrefHeight()));
		addActor(Name);
		addActor(Won);
		addActor(Lost);
		if (client != null)
			influencePool = new Pool<HandleButtonPress>() {

				@Override
				protected HandleButtonPress newObject() {
					return new HandleButtonPress();
				}

			};
		else
			Won.setText("0.00");
	}

	public void start(BrainSurvival g) {
		Name.remove();
		Won.remove();
		Lost.remove();
		addActor(exit);
		addActor(timerImage);
		game = g;
		exit.addListener(new InputListener() {

			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				currentAdd.reset();
				game.setScreen(new Main_Menu(game, false));
				if (client != null) {
					int loss;
					loss = prefs.getInteger("Lost");
					prefs.putInteger("Lost", loss + 1);
					prefs.flush();
					client.endConnection();
					game.getResourceFactory().setClient(null);
				} else
					game.submitScore(false, (int) (allTime * 1000));
				game.showAds();
				dispose();
				Timer.instance().clear();
			}
		});
		currentAdd = microGameArray.random();
		Gdx.app.debug("ChangeGame", "This is the gamecode i am sending: "
				+ microGameArray.indexOf(currentAdd, false));
		findInterface(currentAdd);
		addActor(currentAdd);
		if (client != null) {
			client.sendChangeGame(microGameArray.indexOf(currentAdd, false),
					points, elapsedTime);
		} else {
			Won.setPosition(0, 640);
			addActor(Won);
		}

		currentAdd.init();
	}

	public void pickNewGame(boolean won) {
		if (stillTime(0)) {
			changePoint(won);
			int index = microGameArray.indexOf(currentAdd, false);
			MicroGame temp = currentAdd;
			currentAdd.remove();
			currentAdd.reset();
			microGameArray.removeValue(currentAdd, false);
			currentAdd = microGameArray.random();
			microGameArray.insert(index, temp);
			findInterface(currentAdd);
			addActor(currentAdd);
			if (client != null)
				client.sendChangeGame(
						microGameArray.indexOf(currentAdd, false), points,
						elapsedTime);
			Gdx.app.debug("ChangeGame", "This is the gamecode i am sending: "
					+ microGameArray.indexOf(currentAdd, false));
			currentAdd.init();
		}
	}

	private void changePoint(boolean won) {
		if (won) {
			if (points != 3) {
				addActor(pointHolder.get(points++));
			}
		}

	}

	public void addTime(float time) {
		if (time < 0)
			time *= 2.5;
		elapsedTime -= time;
		stillTime(0);
	}

	public boolean stillTime(float delta) {
		elapsedTime += delta;
		logger.log();
		percent = (Fulltimer - elapsedTime) / Fulltimer;
		if (percent > 0) {
			allTime += delta;
			Won.setText(df.format(allTime));
			if (percent > 1) {
				percent = 1;
				elapsedTime = 0;
			}
			timerImage.setWidth(FullWidth * percent);
			return true;
		} else {
			timerImage.setWidth(0);
			gameOver(false);
			return false;
		}
	}

	public boolean render(float delta) {
		switch (currentInterface) {
		case PHYSICS:
			if (stillTime(delta)) {
				((PhysicsMovable) currentAdd).update();
				return true;
			} else
				return false;
		case DEMO:
			return ((Demo) currentAdd).render(delta);
		case NONE:
			return stillTime(delta);
		default:
			return true;
		}
	}

	private void findInterface(Group c) {
		if (c instanceof PhysicsMovable)
			currentInterface = interFaces.PHYSICS;
		else if (c instanceof Demo)
			currentInterface = interFaces.DEMO;
		else
			currentInterface = interFaces.NONE;
	}

	public void setStep() {
		((PhysicsMovable) currentAdd).setStep();
	}

	public interFaces getInterface() {
		return currentInterface;
	}

	public void gameOver(boolean winner) {

		currentInterface = interFaces.GAMEOVER;
		currentAdd.remove();
		currentAdd.reset();
		for (Image i : pointHolder)
			i.remove();
		timerImage.remove();
		if (client != null)
			exit.remove();
		else {
			if (allTime >= 120) {
				game.unlockAchievements(3);
				game.unlockAchievements(2);
				game.unlockAchievements(1);
			} else if (allTime >= 60) {
				game.unlockAchievements(2);
				game.unlockAchievements(1);
			} else if (allTime >= 30) {
				game.unlockAchievements(1);
			}
		}
		addActor(Name);
		addActor(Won);
		addActor(Lost);
		addActor(Place);
		if (winner) {
			Place.setText("Winner");
			game.submitScore(true, (int) (allTime * 1000));
		} else {
			if (client != null)
				Place.setText("Loser");
			else
				Place.remove();
		}

		Place.setPosition((float) (300 - .5 * Name.getPrefWidth()),
				(float) (400 - .5 * Name.getPrefHeight()));

		if (client != null) {
			int win, loss;
			win = prefs.getInteger("Won");
			loss = prefs.getInteger("Lost");
			if (winner)
				prefs.putInteger("Won", win + 1);
			else
				prefs.putInteger("Lost", loss + 1);

			prefs.flush();
			Lost.setText("LOST: " + prefs.getInteger("Lost"));
			Won.setText("WON: " + prefs.getInteger("Won"));
			Won.setPosition((float) (300 - .5 * Won.getPrefWidth()),
					(float) (305 - .5 * Won.getPrefHeight()));
			Lost.setPosition((float) (300 - .5 * Lost.getPrefWidth()),
					(float) (255 - .5 * Lost.getPrefHeight()));
			addActor(Lost);
			addActor(Won);
		} else {
			Won.setText("Time: " + df.format(allTime));
			Won.setPosition((float) (300 - .5 * Won.getPrefWidth()),
					(float) (305 - .5 * Won.getPrefHeight()));
			Lost.remove();
		}

	}

	public boolean enoughPoints(int i) {
		if (points >= i) {
			points -= i;
			for (Image image : pointHolder) {
				image.remove();
			}
			for (int k = 0; k < points; k++)
				addActor(pointHolder.get(k));
			return true;
		} else {
			// flash
			return false;
		}

	}

	public void setPlayerColor(Color c) {
		this.color = c;
		Place.setColor(c);
		timerImage.setColor(c);
		Lost.getStyle().fontColor = this.color;
		for (Image s : pointHolder) {
			s.setColor(color);
		}
		exit.getStyle().fontColor = c;
	}

	public Color getPlayerColor() {
		return color;
	}

	@Override
	public void setPosition(float x, float y) {
		((ColorMatchingGroup) microGameArray.get(1)).changePos(x, y);
		super.setPosition(x, y);
	}

	public KryoClient getClient() {
		return client;
	}

	public void handleInfluence(int gC, final int button, Color c) {

		if (currentAdd.equals(microGameArray.get(gC))) {
			timerImage.addAction(sequence(color(c), delay(1, color(color)),
					delay(1, color(c)), delay(1, color(color))));
			HandleButtonPress hbp = influencePool.obtain();
			hbp.init(button);
			Gdx.app.postRunnable(hbp);
		}

	}

	public void dispose() {
		((PhysicsMovable) microGameArray.get(1)).dispose();

	}

	private class HandleButtonPress implements Poolable, Runnable {

		int button;

		public void init(int button) {
			this.button = button;
		}

		@Override
		public void run() {
			currentAdd.handleButtonPress(button);
			influencePool.free(this);
		}

		@Override
		public void reset() {
			button = -1;

		}

	}
}
