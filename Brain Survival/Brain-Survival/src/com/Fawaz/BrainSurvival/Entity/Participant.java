package com.Fawaz.BrainSurvival.Entity;

import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.ColorMatchingImagePacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.MiddleButtonPacket;
import com.Fawaz.BrainSurvival.Net.PuppetGroups.PuppetAlgorithm;
import com.Fawaz.BrainSurvival.Net.PuppetGroups.PuppetColorMatching;
import com.Fawaz.BrainSurvival.Net.PuppetGroups.PuppetCupGame;
import com.Fawaz.BrainSurvival.Net.PuppetGroups.PuppetSimonSays;
import com.Fawaz.BrainSurvival.Utils.Demo;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;

public class Participant extends Group {

	private String Username;
	private Image timerImage;
	private Array<Image> pointHolder;
	private float elapsedTime, percent, Fulltimer = 30, FullWidth = 500;
	private MicroGame currentAdd;
	private Array<MicroGame> microGameArray;
	private TextButton name;
	private int points = 0;
	private int winCount = 0;
	private int lossCount = 0;
	private Label Won, Lost, Name, Place;
	private Color color;

	public Participant(float x, float y, Color c, final Player p,
			ResourceFactory r) {
		setSize(640, 640);
		y += 40;
		this.color = new Color(c);
		pointHolder = new Array<Image>(3);
		Image s;
		for (int i = 0; i < 3; i++) {
			s = new Image(r.getTRDfromCircle());
			s.setColor(c);
			s.setBounds(600 - (i * 40), 0, 40, 40);
			pointHolder.add(s);
		}
		timerImage = new Image(r.getTRDfromBox());
		TextButtonStyle tbs = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		// Get name;
		tbs.fontColor = color;
		name = new TextButton("", tbs);
		name.setBounds(0, 0, name.getLabel().getPrefWidth(), 40);
		microGameArray = new Array<MicroGame>();
		microGameArray.add(new PuppetAlgorithm(color, p, r));
		microGameArray.add(new PuppetColorMatching(p, r));
		microGameArray.add(new PuppetSimonSays(p, r));
		microGameArray.add(new PuppetCupGame(color, p, r));
		timerImage.setPosition(75, 300);
		timerImage.setSize(FullWidth, 10f);
		timerImage.setColor(color);
		LabelStyle ls32 = new LabelStyle(r.getFont32(), c);
		Place = new Label("", ls32);
		Place.setPosition((float) (250 - .5 * Place.getPrefWidth()),
				(float) (400 - .5 * Place.getPrefHeight()));
		Won = new Label("", ls32);
		Lost = new Label("", ls32);
		Name = new Label(Username, ls32);
		addActor(Name);
		Won.setPosition((float) (300 - .5 * Won.getPrefWidth()),
				(float) (355 - .5 * Won.getPrefHeight()));
		Lost.setPosition((float) (300 - .5 * Lost.getPrefWidth()),
				(float) (305 - .5 * Lost.getPrefHeight()));
		addActor(Won);
		addActor(Lost);
		currentAdd = microGameArray.random();
		currentAdd.init();
	}

	public void start() {
		addActor(name);
		Name.remove();
		Won.remove();
		Lost.remove();
		addActor(timerImage);
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
		name.setText(username);
		name.setPosition((float) (0 + .5 * name.getPrefWidth()), 0);
		Name.setText(Username);
		Name.setPosition((float) (300 - .5 * Name.getPrefWidth()),
				(float) (350 - .5 * Name.getPrefHeight()));

	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
		for (Image image : pointHolder) {
			image.remove();
		}
		for (int k = 0; k < points; k++)
			addActor(pointHolder.get(k));
	}

	public void setLoss(int loss) {
		lossCount = loss;
		Lost.setText("Lost: " + lossCount);
		Lost.setPosition((float) (300 - .5 * Lost.getPrefWidth()),
				(float) (255 - .5 * Lost.getPrefHeight()));
	}

	public void setPlayerColor(Color color) {
		this.color = new Color(color);
		timerImage.setColor(this.color);
		Lost.getStyle().fontColor = this.color;
		name.getStyle().fontColor = color;
		for (Image s : pointHolder) {
			s.setColor(color);
		}
	}

	public void setWin(int win) {
		winCount = win;
		Won.setText("Won: " + winCount);
		Won.setPosition((float) (300 - .5 * Won.getPrefWidth()),
				(float) (305 - .5 * Won.getPrefHeight()));
	}

	public void changeGame(int i, int points, float elapsedTime) {
		Gdx.app.debug("Change Game", "The game i am changing to: " + i);
		currentAdd.reset();
		currentAdd.remove();
		currentAdd = microGameArray.get(i);
		addActor(currentAdd);
		currentAdd.init();
		this.points = points;
		for (Image image : pointHolder)
			image.remove();
		for (int k = 0; k < points; k++)
			addActor(pointHolder.get(k));

		this.elapsedTime = elapsedTime;
		stillTime(0);
	}

	public void render(float delta) {
		if (currentAdd instanceof Demo) {
			if (((Demo) currentAdd).render(delta))
				stillTime(delta);
		} else
			stillTime(delta);

	}

	public void stillTime(float delta) {
		elapsedTime += delta;
		percent = (Fulltimer - elapsedTime) / Fulltimer;
		if (percent > 0) {
			if (percent > 1) {
				percent = 1;
				elapsedTime = 0;
			}
			timerImage.setWidth(FullWidth * percent);
		} else {
			timerImage.setWidth(0);
		}
	}

	public void gameOver(boolean winner) {
		currentAdd.remove();
		addActor(Name);
		addActor(Won);
		addActor(Lost);
		addActor(Place);
		timerImage.remove();
		for (Image i : pointHolder)
			i.remove();
		name.remove();
		if (winner) {
			Place.setText("Winner");
			setWin(winCount + 1);
		} else {
			Place.setText("Loser");
			setLoss(lossCount + 1);
		}
	}

	public void oppDisconnect() {
		gameOver(false);
		Place.setText("Disconnected");
	}

	public void changeMiddleButtons(MiddleButtonPacket mbp) {
		if (currentAdd.equals(microGameArray.get(0))) {
			((PuppetAlgorithm) currentAdd).changeMiddleButton(mbp);
		}
	}

	public void setImageStatus(ColorMatchingImagePacket[] image) {
		if (currentAdd.equals(microGameArray.get(1))) {
			((PuppetColorMatching) currentAdd).setImageStatus(image);
		}
	}

	public void changeLabelColor(int colorChoice) {
		if (currentAdd.equals(microGameArray.get(1))) {
			((PuppetColorMatching) currentAdd).changeLabelColor(colorChoice);
		}
	}

	public void setSimonOrder(IntArray iA) {
		if (currentAdd.equals(microGameArray.get(2))) {
			((PuppetSimonSays) currentAdd).setSimonOrder(iA);
		}

	}

	public void setCupStatus(int First, int Second) {
		if (currentAdd.equals(microGameArray.get(3))) {
			((PuppetCupGame) currentAdd).switchCups(First, Second);
		}

	}

	public void handleButtonPress(int gC, int index) {
		if (currentAdd.equals(microGameArray.get(gC))) {
			currentAdd.handleButtonPress(index);
		}
		if (gC == 1)
			elapsedTime -= 7;
	}

	public Color getPlayerColor() {
		return color;
	}

}
