package com.Fawaz.BrainSurvival;

import com.Fawaz.BrainSurvival.Screens.SplashScreen;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.Game;

public class BrainSurvival extends Game {

	private ResourceFactory r;
	private ThirdPartyInterface thirdParyInterface;

	public BrainSurvival(ThirdPartyInterface a) {
		thirdParyInterface = a;
	}

	@Override
	public void create() {
		r = new ResourceFactory();
		thirdParyInterface.Login();
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		r.dispose();
		super.dispose();
	}

	public ResourceFactory getResourceFactory() {
		return r;
	}

	public void showAds() {
		thirdParyInterface.showAds();
	}

	public void submitScore(boolean isMultiplayer, int score) {
		if (thirdParyInterface.getSignedIn())
			thirdParyInterface.submitScore(isMultiplayer, score);
	}

	public void displayLeaderBoards() {
		if (thirdParyInterface.getSignedIn())
			thirdParyInterface.showLeaderBoards();
	}

	public void displayAchievements() {
		if (thirdParyInterface.getSignedIn())
			thirdParyInterface.displayAchievements();
	}

	public void unlockAchievements(int index) {
		if (thirdParyInterface.getSignedIn())
			thirdParyInterface.unlockAchievements(index);
	}

	public boolean getSignIn() {
		return thirdParyInterface.getSignedIn();
	}

}
