package com.Fawaz.BrainSurvival.MicroGames.Memory.Model;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MusicImage extends Image {
	private Sound sound;
	private Color c;

	public MusicImage(TextureRegionDrawable s) {
		super(s);
	}

	public void click(boolean f) {
		super.setColor(Color.WHITE);
		if (f)
			sound.play(.5f);

	}

	public void unClick() {
		setColor(c);
	}

	public void setSound(Sound sound) {
		this.sound = sound;
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
		super.setColor(c);
	}

	public Color getUpColor() {
		return c;
	}

}