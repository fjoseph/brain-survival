package com.Fawaz.BrainSurvival.MicroGames.Memory.Model;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Memory.Controller.SimonSaysLogic;
import com.Fawaz.BrainSurvival.Utils.Demo;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class SimonSaysGroup extends MicroGame implements Demo {

	private Array<Sound> soundFiles;
	private Array<Color> colorArray;
	private Array<MusicImage> musicHolder;
	private SimonSaysLogic logic;
	private Player p;
	private Label status;

	public SimonSaysGroup(Player p, ResourceFactory r) {
		setBounds(0, 40, 640, 640);
		this.p = p;
		soundFiles = r.getSoundFiles();
		colorArray = new Array<Color>();
		musicHolder = new Array<MusicImage>();
		LabelStyle ls = new LabelStyle(r.getFont32(), p.getPlayerColor());
		status = new Label("", ls);
		addActor(status);
		colorArray.add(Color.GREEN);
		colorArray.add(Color.BLUE);
		colorArray.add(Color.RED);
		colorArray.add(Color.YELLOW);
		CreateWorld(r);
	}

	private void CreateWorld(ResourceFactory r) {

		int i = 50;
		int j = 0;
		TextureRegionDrawable ud;
		for (j = 0; j < 4; j++) {
			ud = r.getTRDfromCircle();
			MusicImage mb = new MusicImage(ud);
			if (j == 1 || j == 3)
				mb.setBounds(i, 100, 100, 100);
			else
				mb.setBounds(i, 420, 100, 100);

			i = i + 155;
			musicHolder.add(mb);
			addActor(mb);
		}
		i = 0;
		for (MusicImage m : musicHolder) {
			m.setColor(colorArray.get(i));
			m.setSound(soundFiles.get(i));
			i++;
		}
		logic = new SimonSaysLogic(this, p);
	}

	public Array<MusicImage> getMusicHolder() {
		return musicHolder;
	}

	@Override
	public boolean render(float delta) {
		return logic.render(delta);
	}

	@Override
	public void init() {
		logic.createOrder();
		status.setText("Watch");
		status.setBounds((float) (270 - (.5 * status.getPrefWidth())), 350,
				status.getPrefWidth(), status.getPrefHeight());
	}

	@Override
	public void reset() {

	}

	public void changeStatus() {
		status.setText("Replicate");
		status.setBounds((float) (270 - (.5 * status.getPrefWidth())), 350,
				status.getPrefWidth(), status.getPrefHeight());
	}

	@Override
	public void handleButtonPress(final int index) {
		logic.Check(index);
		musicHolder.get(index).click(true);
		Timer.schedule(new Task() {

			@Override
			public void run() {
				musicHolder.get(index).unClick();

			}
		}, .4f);
	}

}
