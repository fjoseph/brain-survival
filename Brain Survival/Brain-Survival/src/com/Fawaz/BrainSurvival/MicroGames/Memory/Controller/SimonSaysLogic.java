package com.Fawaz.BrainSurvival.MicroGames.Memory.Controller;

import java.util.Random;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Memory.Model.MusicImage;
import com.Fawaz.BrainSurvival.MicroGames.Memory.Model.SimonSaysGroup;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class SimonSaysLogic {

	SimonSaysGroup world;
	IntArray order;
	Random rand;
	Player p;
	int buttonPresses;
	int max = 5;
	int k;
	KryoClient client;

	public SimonSaysLogic(SimonSaysGroup world, Player p) {
		this.p = p;
		rand = new Random();
		this.world = world;
		order = new IntArray(5);
		client = p.getClient();
		for (MusicImage mi : world.getMusicHolder()) {
			mi.addListener(new MusicalListener(mi));
		}
	}

	public void createOrder() {
		buttonPresses = 0;
		k = 0;
		for (int i = 0; i < max; i++) {
			order.add(rand.nextInt(4));
		}
		float j = 0;
		for (int i = 0; i < max; i++) {
			world.getMusicHolder().get(order.get(i));
			j += .5;
			Timer.schedule(new Click(world.getMusicHolder().get(order.get(i))),
					j);
			j += .5;
			Timer.schedule(
					new UnClick(world.getMusicHolder().get(order.get(i))), j);
		}
		if (client != null)
			client.sendSimonOrder(order);

	}

	public boolean render(float delta) {
		if (k == 5) {
			return p.stillTime(delta);
		}
		return true;
	}

	public void Check(int pressed) {
		if (k == 5) {
			if (pressed == order.get(buttonPresses)) {
				p.addTime(1.5f);
				if (++buttonPresses == max) {
					order.clear();
					p.pickNewGame(true);
				}
			} else {
				order.clear();
				p.addTime(-1.5f);
				p.pickNewGame(false);

			}
		}
	}

	public class Click extends Task {
		MusicImage mi;

		public Click(MusicImage mi) {
			this.mi = mi;
		}

		@Override
		public void run() {
			mi.click(true);
		}

	}

	public class UnClick extends Task {
		MusicImage mi;

		public UnClick(MusicImage mi) {
			this.mi = mi;
		}

		@Override
		public void run() {
			mi.unClick();
			if (++k == 5)
				world.changeStatus();

		}

	}

	public class MusicalListener extends InputListener {
		private MusicImage mb;

		public MusicalListener(MusicImage mb) {
			this.mb = mb;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			if (k == 5) {
				mb.click(true);
				if (client != null)
					client.sendButtonPress(2,
							world.getMusicHolder().indexOf(mb, false));
				Check(world.getMusicHolder().indexOf(mb, false));
			}
			return true;
		}

		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			if (k == 5)
				mb.unClick();
		}

	}
}