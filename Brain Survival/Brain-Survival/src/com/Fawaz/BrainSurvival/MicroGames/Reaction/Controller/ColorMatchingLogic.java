package com.Fawaz.BrainSurvival.MicroGames.Reaction.Controller;

import java.util.Random;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Reaction.Model.ColorMatchingGroup;
import com.Fawaz.BrainSurvival.MicroGames.Reaction.Model.ColorMatchingGroup.ProbabilityMachine;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class ColorMatchingLogic {

	ColorMatchingGroup world;
	int max = 4;
	int min = 1;
	Random rand;
	Label label;
	Array<Image> imageHolder;
	Array<Body> circleHolder;
	Player p;
	ChangeLabel clc;

	public ColorMatchingLogic(ColorMatchingGroup world, Player p) {
		this.p = p;
		this.world = world;
		this.circleHolder = world.getCirclebodyHolder();
		label = world.getColorRingGirl();
		rand = new Random();
		clc = new ChangeLabel(p.getClient());
	}

	public int SetLabel() {

		int choice = rand.nextInt(max - min + 1) + min;
		switch (choice) {
		case 1:
			label.setText("Green".toUpperCase());
			label.setColor(Color.GREEN);
			break;
		case 2:
			label.setText("Red".toUpperCase());
			label.setColor(Color.RED);
			break;
		case 3:
			label.setText("Yellow".toUpperCase());
			label.setColor(Color.YELLOW);
			break;
		case 4:
			label.setText("Blue".toUpperCase());
			label.setColor(Color.BLUE);
			break;
		}
		return choice;
	}

	public void SetContact() {
		imageHolder = world.getImageHolder();
		for (int i = 0; i < 3; i++) {
			imageHolder.get(i).addListener(
					new buttonListener(imageHolder.get(i)));
		}
	}

	public void startColorChanger() {
		Timer.schedule(clc, 6, 6);
	}

	public void checkBodyArray(Color c) {
		switch (circleHolder.size) {
		case 0:
			clc.cancel();
			p.pickNewGame(true);
			break;
		case 1:
			for (ProbabilityMachine p : ProbabilityMachine.values()) {
				if (c.equals(p.getColor()))
					p.setPercent(.37f);
				else
					p.setPercent(.21f);
			}

			break;
		case 2:
			for (ProbabilityMachine p : ProbabilityMachine.values()) {
				if (c.equals(p.getColor()))
					p.setPercent(.55f);
				else
					p.setPercent(.15f);
			}
			break;

		}
	}

	public class buttonListener extends InputListener {
		SpriteDrawable temp = new SpriteDrawable();
		Image I;

		public buttonListener(Image I) {
			this.I = I;
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			if (label.getColor().equals(I.getColor())) {
				p.addTime(7);
				I.remove();
				world.Destroy(I);
				checkBodyArray(label.getColor());
				world.sendButtonPress();
			}
			return true;
		}

	}

	private class ChangeLabel extends Task {
		Color c = label.getColor();
		private KryoClient client;

		public ChangeLabel(KryoClient client) {
			this.client = client;
		}

		@Override
		public void run() {
			int choice = SetLabel();
			if (client != null)
				client.sendLabelColor(choice);
			switch (circleHolder.size) {
			case 1:
				for (ProbabilityMachine p : ProbabilityMachine.values()) {
					if (c.equals(p.getColor()))
						p.setPercent(.37f);
					else
						p.setPercent(.21f);
				}

				break;
			case 2:
				for (ProbabilityMachine p : ProbabilityMachine.values()) {
					if (c.equals(p.getColor()))
						p.setPercent(.55f);
					else
						p.setPercent(.15f);
				}
				break;

			}

		}

	}

}
