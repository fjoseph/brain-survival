package com.Fawaz.BrainSurvival.MicroGames.Reaction.Model;

import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Box2DBoxCreator extends Box2DBaseObject{
	
	PolygonShape poly;
	float density = 1;
	
	public Box2DBoxCreator(){
		super();
		poly = new PolygonShape();
	}
	
	public void setBox(float w, float h){
		poly.setAsBox(w, h);
	}
	
	public void setDensity(float density){
		this.density = density;
	}
	
	public void createFixture(){
		body.createFixture(poly, density);
	}
	
	public void dispose(){
		super.dispose();
		poly.dispose();
	}
	
}
