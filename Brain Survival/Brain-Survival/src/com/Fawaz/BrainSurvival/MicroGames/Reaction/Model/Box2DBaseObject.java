package com.Fawaz.BrainSurvival.MicroGames.Reaction.Model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;

public abstract class Box2DBaseObject {

	BodyDef def;
	Body body;
	
	
		public Box2DBaseObject() {
			def = new BodyDef();
		}
	
	
	
		public void DefineBody(BodyDef.BodyType type, Vector2 position){
			this.def.type = type;
			def.position.set(position);
			
		}
	
	
	
		public Body getBody() {
			return body;
		}
	
	
	
		public BodyDef getDef() {
			return def;
		}
	
	
	
		public void setBody(Body body) {
			this.body = body;
		}
		
		
		public void dispose(){
			def = null;
			body = null;
		}
		
		public void setData(Sprite sprite){
			body.setUserData(sprite);
		}
	}
	
	
