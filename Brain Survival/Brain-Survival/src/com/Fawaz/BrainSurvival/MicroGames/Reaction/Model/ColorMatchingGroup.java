package com.Fawaz.BrainSurvival.MicroGames.Reaction.Model;

import java.util.Random;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Reaction.Controller.ColorMatchingLogic;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.PhysicsMovable;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class ColorMatchingGroup extends MicroGame implements ContactListener,
		PhysicsMovable {
	private Box2DBoxCreator boxCreator;
	private Box2DCircleCreator circleCreator;
	private Array<Image> imageHolder;
	private Array<Body> circlebodyHolder;
	private Label ColorRingGirl;
	private float Scale = .24257f;
	private float radius = .121285f * 1.5f;
	private float hs = 1.21285f;
	private float BOX_TO_WORLD = 263.841365379f;
	private float WORLD_TO_BOX = 0.00379015625f;
	private KryoClient client;
	private BallColorChanger bcc;
	private World world;
	private Random rand;
	private int randomNum;
	private int max = 3;
	private int min = 1;
	private float yoffset;
	private float xoffset;
	private ColorMatchingLogic logic;
	private Player p;
	private TextureRegionDrawable trd;
	private PositionSender posSender;

	public enum ProbabilityMachine {
		RED(.25, Color.RED, (byte) 2), GREEN(.25, Color.GREEN, (byte) 1), BLUE(
				.25, Color.BLUE, (byte) 4), YELLOW(.25, Color.YELLOW, (byte) 3);

		double percent;
		Color color;
		byte code;

		ProbabilityMachine(double percent, Color color, byte code) {
			this.percent = percent;
			this.color = color;
			this.code = code;
		}

		public void setPercent(float percent) {
			this.percent = percent;
		}

		public Color getColor() {
			return color;
		}

		public double getPercent() {
			return percent;
		}

		public byte getCode() {
			return code;
		}

	}

	public ColorMatchingGroup(float x, float y, Player p, ResourceFactory r) {
		setBounds(0, 40, 640, 640);
		this.p = p;
		posSender = new PositionSender();
		rand = new Random();
		xoffset = convertToBox(x);
		yoffset = convertToBox(y);
		LabelStyle ColorRingGirlStyle = new LabelStyle();
		bcc = new BallColorChanger();
		imageHolder = new Array<Image>(3);
		circlebodyHolder = new Array<Body>(3);
		BitmapFont font = r.getFont32();
		ColorRingGirlStyle.font = font;
		ColorRingGirl = new Label("", ColorRingGirlStyle);
		trd = r.getTRDfromCircle();
		ColorRingGirl.setFontScale(1.5f);
		ColorRingGirl.setPosition(0, 520);
		ColorRingGirl.setWidth(640);
		ColorRingGirl.setAlignment(Align.center);

		addActor(ColorRingGirl);
		world = new World(new Vector2(0, -6), false);
		world.setContactListener(this);
		client = p.getClient();
		logic = new ColorMatchingLogic(this, p);

	}

	public void changePos(float x, float y) {
		xoffset = convertToBox(x);
		yoffset = convertToBox(y);
	}

	private void CreateWorld() {
		Body body;
		Image image;
		for (int i = 0; i < 3; i++) {
			imageHolder.add(new Image(trd));
		}
		boxCreator = new Box2DBoxCreator();
		circleCreator = new Box2DCircleCreator();

		for (int i = 1; i <= 3; i++) {
			circleCreator.DefineBody(BodyType.DynamicBody, new Vector2(xoffset
					+ ((i * 2.5f) * Scale), yoffset + (8 * Scale)));
			body = world.createBody(circleCreator.getDef());
			circleCreator.setBody(body);
			circleCreator.setRadius(radius);
			circleCreator.defineFixture(.2f, .5f, 1f);
			circleCreator.createFixture();
			body = circleCreator.getBody();
			image = imageHolder.get(i - 1);
			body.setUserData(image);
			body.setLinearDamping(1f);

			image.setBounds(convertToWorld(body.getPosition().x - xoffset)
					- image.getWidth() / 2, convertToWorld(body.getPosition().y
					- yoffset)
					- image.getHeight() / 2,
					convertToWorld(circleCreator.getRadius() * 2),
					convertToWorld(circleCreator.getRadius() * 2));
			addActor(image);
			circlebodyHolder.add(body);
		}
		circleCreator.dispose();

		boxCreator.DefineBody(BodyType.StaticBody, new Vector2(xoffset + hs,
				yoffset + 0));
		body = world.createBody(boxCreator.getDef());
		boxCreator.setBody(body);
		boxCreator.setBox(hs, 0);
		boxCreator.createFixture();

		boxCreator.DefineBody(BodyType.StaticBody, new Vector2(xoffset + hs,
				yoffset + (hs * 2)));
		body = world.createBody(boxCreator.getDef());
		boxCreator.setBody(body);
		boxCreator.createFixture();

		boxCreator.DefineBody(BodyType.StaticBody, new Vector2(xoffset + 0,
				yoffset + hs));
		body = world.createBody(boxCreator.getDef());
		boxCreator.setBody(body);
		boxCreator.setBox(0, hs);
		boxCreator.createFixture();

		boxCreator.DefineBody(BodyType.StaticBody, new Vector2(xoffset
				+ (hs * 2), yoffset + hs));
		body = world.createBody(boxCreator.getDef());
		boxCreator.setBody(body);
		boxCreator.createFixture();

		boxCreator.dispose();
		for (ProbabilityMachine p : ProbabilityMachine.values()) {
			p.setPercent((float) .25);
		}
	}

	public void update() {
		Image tempI;
		for (Body b : circlebodyHolder) {
			tempI = (Image) b.getUserData();
			tempI.setPosition(convertToWorld(b.getPosition().x - xoffset)
					- tempI.getWidth() / 2, convertToWorld(b.getPosition().y
					- yoffset)
					- tempI.getHeight() / 2);

		}
	}

	private float convertToBox(float x) {
		return x * WORLD_TO_BOX;
	}

	private float convertToWorld(float x) {
		return x * BOX_TO_WORLD;
	}

	public Label getColorRingGirl() {
		return ColorRingGirl;
	}

	public void setStep() {
		if (world != null)
			world.step(1 / 45f, 6, 2);

	}

	public float getBOX_TO_WORLD() {
		return BOX_TO_WORLD;
	}

	public Array<Image> getImageHolder() {
		return imageHolder;
	}

	public void Destroy(Image i) {
		imageHolder.removeValue(i, false);
		for (Body bo : circlebodyHolder) {
			if (i.equals((Image) bo.getUserData())) {
				world.destroyBody(bo);
				circlebodyHolder.removeValue(bo, false);
				break;
			}
		}
	}

	public Array<Body> getCirclebodyHolder() {
		return circlebodyHolder;
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture f1 = contact.getFixtureA();
		Fixture f2 = contact.getFixtureB();

		if (f1.getShape().getType().name().equals("Circle")
				&& f1.getBody().getPosition().y < .2f + yoffset) {
			randomNum = rand.nextInt(max - min + 1) + min;
			if (f1.getBody().getLinearVelocity().x < 0)
				randomNum *= -1;
			f1.getBody().setLinearVelocity(randomNum, 6);
		}

		if (f2.getShape().getType().name().equals("Circle")
				&& f2.getBody().getPosition().y < .2f + yoffset) {
			randomNum = rand.nextInt(max - min + 1) + min;
			if (f2.getBody().getLinearVelocity().x < 0)
				randomNum *= -1;
			f2.getBody().setLinearVelocity(randomNum, 6);
		}
	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

	@Override
	public void init() {
		CreateWorld();
		logic.SetContact();
		logic.SetLabel();
		logic.startColorChanger();
		Timer.schedule(bcc, 0, 2);
		if (client != null)
			Timer.schedule(posSender, 0, .06f);
	}

	@Override
	public void reset() {
		bcc.cancel();
		if (client != null)
			posSender.cancel();

	}
	
	@Override
	public void dispose(){
		for (Image i : imageHolder) {
			i.remove();
			Destroy(i);
		}
		world.dispose();
	}

	public class BallColorChanger extends Task {
		boolean running = true;
		double probability;
		Random rand;
		Image temp;

		public BallColorChanger() {
			rand = new Random();
		}

		@Override
		public void run() {
			for (Image i : imageHolder) {
				double probability = rand.nextDouble();
				double sum = 0;
				for (ProbabilityMachine p : ProbabilityMachine.values()) {
					sum += p.getPercent();
					if (probability <= sum) {
						i.setColor(p.getColor());
						break;
					}
				}
			}
		}

	}

	@Override
	public void handleButtonPress(int index) {
		Image i = imageHolder.get(index);
		if (ColorRingGirl.getColor().equals(i.getColor())) {
			p.addTime(7);
		}
		Destroy(i);
		i.remove();
		logic.checkBodyArray(ColorRingGirl.getColor());

	}

	private class PositionSender extends Task {

		@Override
		public void run() {
			client.sendImageStatus(imageHolder);
		}

	}

	public void sendButtonPress() {
		if(client != null)
			client.sendButtonPress(1, 0);
		
	}

}