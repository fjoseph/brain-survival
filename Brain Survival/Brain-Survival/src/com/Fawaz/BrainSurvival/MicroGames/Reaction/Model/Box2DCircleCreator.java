package com.Fawaz.BrainSurvival.MicroGames.Reaction.Model;

import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Box2DCircleCreator extends Box2DBaseObject {
	
	FixtureDef fixtureDef;
	CircleShape circleshape;
	
	public Box2DCircleCreator(){
		super();
		circleshape = new CircleShape();
		fixtureDef = new FixtureDef();
	}
	
	public void setRadius(float r){
		circleshape.setRadius(r);
	}
	
	public float getRadius(){
		return circleshape.getRadius();
	}
	
	public void defineFixture(float density, float friction, float restitution){
		fixtureDef.shape = circleshape;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		fixtureDef.restitution = restitution;
	}
	
	public void createFixture(){
		body.createFixture(fixtureDef);
	}
	
	public void dispose(){
		super.dispose();
		circleshape.dispose();
		fixtureDef = null;
	}
	
}
