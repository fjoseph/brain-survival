package com.Fawaz.BrainSurvival.MicroGames.Visual.Model;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Visual.Controller.CupGameLogic;
import com.Fawaz.BrainSurvival.Utils.Demo;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class CupGameGroup extends MicroGame implements Demo {

	public enum Positions {
		FIRST(120f), SECOND(270f), THIRD(420f);

		float x;
		float y = 140;

		Positions(float x) {
			this.x = x;
		}

		public float getX() {
			return x;
		}

		public float getY() {
			return y;
		}
	}

	private Image image;
	private Array<Color> colorArray;
	private Array<Image> cupArray;
	private CupGameLogic logic;
	private Player p;

	public CupGameGroup(Color c, Player player, ResourceFactory r) {
		p = player;
		setBounds(0, 40, 640, 640);
		cupArray = new Array<Image>();
		colorArray = new Array<Color>();
		colorArray.add(Color.GREEN);
		colorArray.add(Color.BLUE);
		colorArray.add(Color.YELLOW);
		for (int i = 0; i < 3; i++) {
			image = new Image(r.getTRDfromCircle());
			addActor(image);
			image.setName(" ");
			cupArray.add(image);
		}

		image = new Image(r.getTRDfromCircle());
		image.setColor(c);
		addActor(image);
		logic = new CupGameLogic(this, p);

	}

	public Image getObj() {
		return image;
	}

	public Array<Image> getCupArray() {
		return cupArray;
	}

	@Override
	public boolean render(float delta) {
		return logic.render(delta);
	}

	@Override
	public void init() {
		for (int index = 0; index < 3; index++) {
			Image i = cupArray.get(index);
			Positions p = Positions.values()[index];
			Color c = colorArray.get(index);
			i.setName("");
			i.setColor(c);
			i.setPosition(p.getX(), p.getY());
			i.setTouchable(Touchable.enabled);
		}
		image.setBounds(Positions.SECOND.getX() + 30, 410, 50, 50);
		logic.FirstStage();
	}

	@Override
	public void reset() {
	}

	@Override
	public void handleButtonPress(int index) {
		boolean match;
		Image i = cupArray.get(index);
		Color c = new Color(i.getColor());
		if (i.getName().equals("Neo"))
			match = true;
		else
			match = false;
		i.setColor(Color.WHITE);
		Timer.schedule(new unClick(i, c), .1f);
		logic.finalStage(match);
	}

	private class unClick extends Task {
		Image i;
		Color c;

		public unClick(Image i, Color c) {
			this.i = i;
			this.c = c;
		}

		@Override
		public void run() {
			i.setColor(c);
		}

	}

	public Array<Color> getColorArray() {
		return colorArray;
	}

}
