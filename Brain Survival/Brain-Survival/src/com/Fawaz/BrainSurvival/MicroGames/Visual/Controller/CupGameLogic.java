package com.Fawaz.BrainSurvival.MicroGames.Visual.Controller;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.Random;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.MicroGames.Visual.Model.CupGameGroup;
import com.Fawaz.BrainSurvival.MicroGames.Visual.Model.CupGameGroup.Positions;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class CupGameLogic {
	private CupGameGroup world;
	private Random rand;
	private int random, count;
	private Player p;
	private KryoClient c;
	private Array<Color> colorArray;

	public CupGameLogic(CupGameGroup world, Player p) {
		this.p = p;
		this.world = world;
		rand = new Random();
		c = p.getClient();
		colorArray = world.getColorArray();
		for (Image i : world.getCupArray()) {
			i.addListener(new CupListener(i));
		}
	}

	public void FirstStage() {
		count = 0;
		random = rand.nextInt(3);
		world.getCupArray().get(random).setName("Neo");
		if (c != null)
			c.sendButtonPress(3, random * 10 + 3);
		float neoX = 0, neoY = 0;
		switch (random) {
		case 0:
			neoX = Positions.FIRST.getX() + 25;
			neoY = Positions.FIRST.getY() + 25;
			break;
		case 1:
			neoX = Positions.SECOND.getX() + 25;
			neoY = Positions.SECOND.getY() + 25;
			break;
		case 2:
			neoX = Positions.THIRD.getX() + 25;
			neoY = Positions.THIRD.getY() + 25;
			break;
		}
		world.getObj().addAction(sequence(moveTo(neoX, neoY, .5f), alpha(0)));
		world.getObj().toBack();

		int First = -1, Second = -1;
		float l = .5f;
		for (int i = 0; i < 7; i++) {

			while (First == Second) {
				First = rand.nextInt(3);
				Second = rand.nextInt(3);
			}

			Timer.schedule(new Switch(world.getCupArray().get(First), world
					.getCupArray().get(Second)), l);
			l += .3;
			First = -1;
			Second = -1;
		}
	}

	public boolean render(float delta) {

		if (count == 7)
			return p.stillTime(delta);
		return true;
	}

	public void finalStage(final boolean b) {
		world.getObj().setPosition(world.getCupArray().get(random).getX() + 25,
				world.getObj().getY());
		world.getObj().addAction(alpha(1));
		Timer.schedule(new Task() {

			@Override
			public void run() {
				addTime(b);
			}
		}, .5f);
		for (Image i : world.getCupArray()) {
			i.addAction(moveTo(i.getX(), 400, .5f));// move by
		}
	}

	private void addTime(boolean b) {
		if (b) {
			p.addTime(3);
		} else
			p.addTime(-3);
		for (Image i : world.getCupArray()) {
			i.setName("");
		}
		p.pickNewGame(b);
	}

	public class Switch extends Task {
		Image first, second;

		public Switch(Image first, Image second) {
			this.first = first;
			this.second = second;
		}

		@Override
		public void run() {
			//Optimize Timer.schedule delay up to 7 times
			if (c != null)
				c.sendCupStatus(world.getCupArray().indexOf(first, false),
						world.getCupArray().indexOf(second, false));
			first.addAction(moveTo(second.getX(), second.getY(), .2f));
			second.addAction(moveTo(first.getX(), first.getY(), .2f));
			first.setColor(colorArray.random());
			second.setColor(colorArray.random());
			count++;
		}

	}

	private class CupListener extends InputListener {
		private Image i;
		private boolean match;
		private Color clr;

		public CupListener(Image i) {
			this.i = i;
			clr = new Color(i.getColor());
		}

		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			if (count == 7) {
				if (i.getName().equals("Neo"))
					match = true;
				else
					match = false;
				i.setColor(Color.WHITE);
			}
			return true;
		}

		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			if (count == 7) {
				i.setColor(clr);
				if (c != null)
					c.sendButtonPress(3, 2);
				finalStage(match);
			}
		}
	}

}
