package com.Fawaz.BrainSurvival.MicroGames.Logic.Model;

import java.util.Random;

import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.MiddleButtonPacket;
import com.Fawaz.BrainSurvival.Utils.MicroGame;
import com.Fawaz.BrainSurvival.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;

public class ArithmeticGroup extends MicroGame {

	private Array<TextButton> buttons;
	private Player p;
	private KryoClient client;
	private int choice;
	private Random random;

	public ArithmeticGroup(Color c, Player p, ResourceFactory r) {
		setBounds(0, 40, 640, 640);
		buttons = new Array<TextButton>(7);
		TextButtonStyle normal = new TextButtonStyle(null, null, null,
				r.getFont32());
		TextButtonStyle pressable = new TextButtonStyle(null,
				r.getTRDfromBox(), null, r.getFont32());
		normal.fontColor = p.getPlayerColor();
		pressable.fontColor = p.getPlayerColor();
		this.p = p;
		client = p.getClient();
		random = new Random();
		for (int i = 0; i < 7; i++) {
			if (i <= 3) {
				buttons.add(new TextButton("", normal));
				buttons.peek().setBounds(i * 160, 400, 160, 64);
				buttons.peek().setOrigin(buttons.peek().getWidth() / 2,
						buttons.peek().getHeight() / 2);
			} else {
				buttons.add(new TextButton("", pressable));
				buttons.peek().setBounds((i - 4) * 213, 128, 213, 64);
				buttons.peek().setOrigin(buttons.peek().getWidth() / 2,
						buttons.peek().getHeight() / 2);
				buttons.peek().addListener(new checkAnswer(i));
			}
			addActor(buttons.peek());
		}

		buttons.get(1).setText("?");
		buttons.get(2).setText("=");

	}

	@Override
	public void init() {
		int firstNumber, secondNumber, answer;
		String sign;
		for (int i = 4; i < 7; i++) {
			if (random.nextBoolean())
				sign = "+ ";
			else
				sign = "- ";
			buttons.get(i).setText(sign + random.nextInt(101));
		}
		firstNumber = random.nextInt(101);
		answer = random.nextInt(101);
		choice = random.nextInt(3) + 4;
		boolean operation = random.nextBoolean();
		if (operation) {// addition
			secondNumber = firstNumber + answer;
			sign = "+ ";
		} else {
			secondNumber = firstNumber - answer;
			sign = "- ";

		}
		buttons.get(choice).setText(sign + answer);
		buttons.get(0).setText(Integer.toString(firstNumber));
		buttons.get(3).setText(Integer.toString(secondNumber));
		if (client != null) {
			MiddleButtonPacket mbp = new MiddleButtonPacket();
			mbp.firstNumber = buttons.get(0).getText().toString();
			mbp.secondNumber = buttons.get(3).getText().toString();
			mbp.option1 = buttons.get(4).getText().toString();
			mbp.option2 = buttons.get(5).getText().toString();
			mbp.option3 = buttons.get(6).getText().toString();
			client.sendMiddleChange(mbp);
		}
	}

	public void checkAnswer(int index) {
		if (index == choice) {
			p.addTime(3.5f);
			p.pickNewGame(true);
		} else {
			p.addTime(-3.5f);
			p.pickNewGame(false);
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleButtonPress(int index) {
		checkAnswer(index);
	}

	private class checkAnswer extends InputListener {
		int index;

		public checkAnswer(int index) {
			this.index = index;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			checkAnswer(index);
			if (client != null)
				client.sendButtonPress(0, index);
		}
	}

}
