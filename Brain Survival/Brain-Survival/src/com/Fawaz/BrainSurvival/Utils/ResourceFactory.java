package com.Fawaz.BrainSurvival.Utils;

import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public class ResourceFactory {

	private Array<Texture> textureHolder;
	private Array<TextureRegionDrawable> textureDrawableHolder;
	private BitmapFont font12;
	private BitmapFont font32;
	private BitmapFont Devfont;
	private KryoClient client;
	private Array<Sound> soundFiles;
	private Preferences prefs = Gdx.app.getPreferences("Brain_Survival");

	public ResourceFactory() {
		textureHolder = new Array<Texture>(15);
		textureDrawableHolder = new Array<TextureRegionDrawable>(15);
		textureHolder.add(new Texture(Gdx.files.internal("data/Circle.png")));
		textureHolder.add(new Texture(Gdx.files.internal("data/Block.png")));
		textureHolder.add(new Texture(Gdx.files.internal("data/Primary.png")));
		textureHolder
				.add(new Texture(Gdx.files.internal("data/Secondary.png")));
		textureHolder.add(new Texture(Gdx.files.internal("data/Cursor.png")));
		textureHolder.add(new Texture(Gdx.files.internal("data/Red.png")));

		textureHolder.add(new Texture(Gdx.files
				.internal("data/Smiley_Face.png")));

		for (int i = 1; i <= 9; i++) {
			textureHolder.add(new Texture(Gdx.files.internal("data/ScreenShot/"
					+ i + ".png")));
		}

		TextureRegion tr;
		for (Texture t : textureHolder) {
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(t);
			textureDrawableHolder.add(new TextureRegionDrawable(tr));
		}
		// Textures

		// Font 32
		Texture buttonTexture = new Texture(
				Gdx.files.internal("data/32Font.png"));
		buttonTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		// buttonTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font32 = new BitmapFont(Gdx.files.internal("data/32Font.fnt"),
				new TextureRegion(buttonTexture), false);

		// Font 12
		Texture buttonTexture12 = new Texture(
				Gdx.files.internal("data/12Font.png"));
		buttonTexture12.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font12 = new BitmapFont(Gdx.files.internal("data/12Font.fnt"),
				new TextureRegion(buttonTexture12), false);

		// Dev Font
		Texture btDev = new Texture(Gdx.files.internal("data/DevFont32_0.png"));
		btDev.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		Devfont = new BitmapFont(Gdx.files.internal("data/DevFont32.fnt"),
				new TextureRegion(btDev), false);

		soundFiles = new Array<Sound>(4);
		soundFiles.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sounds/3520__patchen__tone-hit.wav")));
		soundFiles.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sounds/30341__junggle__waterdrop24.wav")));
		soundFiles.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sounds/Grenadine-Pair-2.wav")));
		soundFiles.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sounds/179198__snapper4298__micro-bell.wav")));

	}

	public Texture getCircle() {
		return textureHolder.first();
	}

	public Texture getBox() {
		return textureHolder.get(1);
	}

	public BitmapFont getFont12() {
		return font12;
	}

	public BitmapFont getFont32() {
		return font32;
	}

	public void dispose() {
		for (Texture t : textureHolder)
			t.dispose();
		for (Sound s : soundFiles)
			s.dispose();
		if (client != null) {
			int loss;
			loss = prefs.getInteger("Lost");
			prefs.putInteger("Lost", loss + 1);
			prefs.flush();
			client.endConnection();
		}
	}

	public TextureRegionDrawable getTRDfromBox() {
		return textureDrawableHolder.get(1);
	}

	public TextureRegionDrawable getTRDfromCircle() {
		return textureDrawableHolder.first();
	}

	public TextureRegionDrawable getCursorDrawable() {
		return textureDrawableHolder.get(4);
	}

	public TextureRegionDrawable getSplashSecondary() {
		return textureDrawableHolder.get(3);
	}

	public TextureRegionDrawable getSplashPrimary() {
		return textureDrawableHolder.get(2);
	}

	public TextureRegionDrawable getRed() {
		return textureDrawableHolder.get(5);
	}

	public TextureRegionDrawable getSmiley() {
		return textureDrawableHolder.get(6);
	}

	public void setClient(KryoClient client) {
		this.client = client;
	}

	public BitmapFont getDevFont() {
		return Devfont;
	}

	public TextureRegionDrawable getTutorialDrawable(int i) {
		if (i <= 0 || i > 10 || i == 6)
			return null;
		else if (i > 6)
			i--;
		return textureDrawableHolder.get(6 + i);
	}

	public Array<Sound> getSoundFiles() {
		return soundFiles;
	}

}