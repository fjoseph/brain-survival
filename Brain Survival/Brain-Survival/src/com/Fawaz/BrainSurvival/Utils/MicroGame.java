package com.Fawaz.BrainSurvival.Utils;

import com.badlogic.gdx.scenes.scene2d.Group;

public abstract class MicroGame extends Group {

	public abstract void init();

	public abstract void reset();

	public abstract void handleButtonPress(int index);
}
