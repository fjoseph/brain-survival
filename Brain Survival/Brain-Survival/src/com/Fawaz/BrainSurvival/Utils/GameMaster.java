package com.Fawaz.BrainSurvival.Utils;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.hide;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.show;

import java.text.DecimalFormat;

import com.Fawaz.BrainSurvival.BrainSurvival;
import com.Fawaz.BrainSurvival.Entity.Participant;
import com.Fawaz.BrainSurvival.Entity.Player;
import com.Fawaz.BrainSurvival.Entity.Player.interFaces;
import com.Fawaz.BrainSurvival.Net.Client.KryoClient;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.ColorMatchingImagePacket;
import com.Fawaz.BrainSurvival.Net.Client.Packets.InGamePackets.MiddleButtonPacket;
import com.Fawaz.BrainSurvival.Screens.Main_Menu;
import com.Fawaz.BrainSurvival.Screens.MultiplayerChooser;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class GameMaster {

	Stage stage;
	Player player;
	Participant opponent;
	BrainSurvival game;
	Label status, PC, timer, enemyReady, roomId;
	Array<Button> Colors = new Array<Button>(8);
	TextButton return_MainMenu, playerReady, return_Chooser;
	private int size = 0, choice;
	Array<Sprite> elipsis = new Array<Sprite>();
	KryoClient client;
	float countdown;
	int seconds;
	DecimalFormat df = new DecimalFormat();
	ResourceFactory r;
	SpriteBatch sb;
	private Image smileyPlayer, smileyOp;
	boolean gameStarted = false, disconnected = true;

	public enum DeepestState {
		SERVER, INGAME, LOBBY, COUNTDOWN, TIMEOUT;
	}

	DeepestState phase;

	public GameMaster(BrainSurvival game, Stage stage, int choice) {
		this.choice = choice;
		this.stage = stage;
		sb = (SpriteBatch) stage.getSpriteBatch();
		this.game = game;
		r = game.getResourceFactory();
		TextButtonStyle tbs = new TextButtonStyle(null, r.getTRDfromBox(),
				null, r.getFont32());
		tbs.fontColor = Color.GREEN;
		return_MainMenu = new TextButton("return to main menu", tbs);
		return_Chooser = new TextButton("Choose a mode", tbs);
		return_Chooser.setBounds(0, 0, return_Chooser.getPrefWidth(),
				return_Chooser.getPrefHeight());
		return_MainMenu
				.setBounds(1280 - return_MainMenu.getPrefWidth(), 0,
						return_MainMenu.getPrefWidth(),
						return_MainMenu.getPrefHeight());
		playerReady = new TextButton("Ready", new TextButtonStyle(tbs));
		playerReady.getStyle().checked = r.getTRDfromBox();
		LabelStyle ls = new LabelStyle(r.getFont32(), Color.GREEN);
		roomId = new Label("", ls);
		roomId.setPosition(530 - (.5f * roomId.getPrefWidth()),
				580 - (.5f * roomId.getPrefHeight()));
		enemyReady = new Label("Ready", new LabelStyle(ls));
		status = new Label("Looking for a Room", ls);
		timer = new Label("", ls);
		status.setBounds(640 - (.5f * status.getPrefWidth()),
				340 - (.5f * status.getPrefHeight()), status.getPrefWidth(),
				status.getPrefHeight());
		stage.addActor(status);
		stage.addActor(return_MainMenu);
		stage.addActor(return_Chooser);
		for (int i = 0; i < 3; i++) {
			elipsis.add(new Sprite(r.getCircle()));
			elipsis.peek().setBounds(780 + (13 * i), 328, 7, 7);
			elipsis.peek().setColor(Color.GREEN);
		}
		ButtonStyle colorsStyle = new TextButtonStyle(r.getTRDfromBox(),
				r.getTRDfromBox(), null, null);
		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.RED);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.BLUE);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.CYAN);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.GREEN);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.MAGENTA);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.ORANGE);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.PINK);

		Colors.add(new Button(colorsStyle));
		Colors.peek().setColor(Color.YELLOW);

		for (Button s : Colors) {
			s.addListener(new colorPicker(s));
		}

		client = new KryoClient(this, choice);
		r.setClient(client);
		player = new Player(0, 0, Color.GREEN, r, client);
		opponent = new Participant(0, 0, Color.GREEN, player, r);
		PC = new Label("Pick a color", ls);
		PC.setSize(PC.getPrefWidth(), PC.getPrefHeight());

		return_MainMenu.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				client.endConnection();
				r.setClient(null);
				if (gameStarted && !disconnected)
					GameMaster.this.game.showAds();
				GameMaster.this.game.setScreen(new Main_Menu(
						GameMaster.this.game, false));

			}
		});

		return_Chooser.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				client.endConnection();
				if (gameStarted && !disconnected)
					GameMaster.this.game.showAds();
				r.setClient(null);
				GameMaster.this.game.setScreen(new MultiplayerChooser(
						GameMaster.this.game));
			}
		});

		playerReady.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				client.sendReady(playerReady.isChecked());
				if (!playerReady.isChecked()) {
					timer.remove();
					phase = DeepestState.LOBBY;
				}
			}
		});

		Timer.schedule(new elli(), 0, .3f);
		phase = DeepestState.SERVER;
		df.setMaximumFractionDigits(2);
		smileyPlayer = new Image(r.getSmiley());
		smileyPlayer.setSize(640, 640);
		smileyOp = new Image(r.getSmiley());
		smileyOp.setSize(640, 640);
		
	}
	
	public void login(){
		client.Login();
	}

	public void initializeLobby() {
		if (phase != DeepestState.COUNTDOWN)
			phase = DeepestState.LOBBY;
		stage.addActor(player);
		stage.addActor(PC);
		for (Button t : Colors) {
			stage.addActor(t);
		}
		status.remove();
		// return_MainMenu.remove();

	}

	public void setRoomID(int choice) {
		roomId.setText("Room id: " + choice);
		stage.addActor(roomId);
	}

	public void render(float delta) {

		switch (phase) {
		case INGAME:
			if (!player.render(delta))
				setGameOver(true);
			stage.act();
			stage.draw();
			if (player.getInterface() == interFaces.PHYSICS)
				player.setStep();
			opponent.render(delta);
			break;
		case SERVER:
			stage.getSpriteBatch().begin();
			for (int i = 0; i < size; i++)
				elipsis.get(i).draw(stage.getSpriteBatch());
			stage.getSpriteBatch().end();
			break;
		case COUNTDOWN:
			stage.draw();
			countdown -= delta;
			timer.setText(df.format(countdown));
			timer.setPosition((float) (640 - .5 * timer.getPrefWidth()),
					(float) (350 - .5 * timer.getPrefHeight()));
			if (countdown <= 0) {
				for (Button b : Colors) {
					b.remove();
				}
				timer.remove();
				return_MainMenu.remove();
				return_Chooser.remove();
				return_MainMenu.setBounds(
						1280 - return_MainMenu.getPrefWidth(), 0,
						return_MainMenu.getPrefWidth(),
						return_MainMenu.getPrefHeight());
				playerReady.remove();
				enemyReady.remove();
				PC.remove();
				roomId.remove();
				phase = DeepestState.INGAME;
				player.start(game);
				opponent.start();
				gameStarted = true;
				if (choice == 1)
					game.unlockAchievements(4);
				else if (choice == 2)
					game.unlockAchievements(6);
				else
					game.unlockAchievements(7);
				// let client initialize the connectable for each client
			}
			break;
		default:
			stage.draw();
			break;

		}
	}

	private void setGameOver(boolean playerOver) {
		if (playerOver) {
			opponent.gameOver(true);
		} else {
			game.unlockAchievements(5);
			player.gameOver(true);
			opponent.gameOver(false);
		}
		client.sendGameOver();
		client.endConnection();
		stage.addActor(return_MainMenu);
		stage.addActor(return_Chooser);

	}

	public void initializePlayer(int i, boolean onlyOne, int color) {
		player.setPosition(i * 640, 0);
		smileyPlayer.setPosition(i * 640, 40);
		player.setPlayerColor(Colors.get(color).getColor());
		smileyOp.setColor(player.getPlayerColor());
		if (i == 0) {
			opponent.remove();
			enemyReady.remove();
		}
		PC.setPosition(player.getX() + 300 - (.5f * PC.getPrefWidth()),
				550 - (.5f * PC.getPrefHeight()));

		for (int j = 0; j < 4; j++) {
			Colors.get(j)
					.setBounds(player.getX() + (180 + j * 70), 470, 50, 50);
		}
		for (int j = 4; j < 8; j++) {
			Colors.get(j).setBounds(player.getX() + (180 + (j - 4) * 70), 410,
					50, 50);
		}

		playerReady
				.setPosition((float) (player.getX() + (300 - .5 * playerReady
						.getPrefWidth())), 160);
		stage.addActor(playerReady);
		player.toBack();
		if (onlyOne) {
			timer.remove();
			phase = DeepestState.LOBBY;
			for (Button b : Colors)
				b.setVisible(true);
		}
	}

	public void initializeOpponent(int i, String username, int Win, int Loss,
			int color, boolean ready) {
		opponent.setPosition(i * 640, 0);
		opponent.setUsername(username);
		opponent.setWin(Win);
		opponent.setLoss(Loss);
		for (Button b : Colors)
			b.setVisible(true);
		opponent.setPlayerColor(Colors.get(color).getColor());
		Colors.get(color).setVisible(false);
		smileyOp.setPosition(i * 640, 40);
		smileyPlayer.setColor(Colors.get(color).getColor());
		stage.addActor(opponent);
		opponent.toBack();
		if (!ready) {
			enemyReady.getStyle().background = null;
			timer.remove();
			phase = DeepestState.LOBBY;
		} else
			enemyReady.getStyle().background = r.getTRDfromBox();
		enemyReady.setPosition(
				(float) (opponent.getX() + (300 - .5 * enemyReady
						.getPrefWidth())), 160);
		stage.addActor(enemyReady);

	}

	public class elli extends Task {

		@Override
		public void run() {
			if (size == 3)
				size = 0;
			else
				size++;
		}
	}

	public class colorPicker extends InputListener {
		private Button b;
		private Color c;

		public colorPicker(Button b) {
			this.b = b;
			c = new Color(b.getColor());
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			if (phase != DeepestState.COUNTDOWN)
				b.setColor(Color.WHITE);
			return true;
		}

		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer,
				int button) {
			if (phase != DeepestState.COUNTDOWN) {
				b.setColor(c);
				player.setPlayerColor(c);
				client.sendColor(Colors.indexOf(b, false));
			}
		}
	}

	public void initializeCountdown(float time) {
		countdown = time;
		phase = DeepestState.COUNTDOWN;
		timer.setText(time / 1000 + "");
		timer.setPosition((float) (640 - .5 * timer.getPrefHeight()),
				(float) (350 - .5 * timer.getPrefWidth()));
		stage.addActor(timer);
	}

	public void changeOpponentGame(int i, int points, float elapsedTime) {
		if (phase == DeepestState.COUNTDOWN)
			countdown = 0;
		opponent.changeGame(i, points, elapsedTime);
	}

	public void changeMiddleButton(MiddleButtonPacket mbp) {
		opponent.changeMiddleButtons(mbp);
	}

	public void setimageMovement(ColorMatchingImagePacket[] image) {
		opponent.setImageStatus(image);
	}

	public void changeLabelColor(int colorChoice) {
		opponent.changeLabelColor(colorChoice);
	}

	public void setSimonOrder(IntArray iA) {
		opponent.setSimonOrder(iA);
	}

	public void setCupStatus(int First, int Second) {
		opponent.setCupStatus(First, Second);

	}

	public void handleGameOver() {
		setGameOver(false);
	}

	public void handleButtonPress(int gC, int button) {
		opponent.handleButtonPress(gC, button);

	}

	public void handeInfluence(int gC, int button) {
		player.handleInfluence(gC, button, opponent.getPlayerColor());
		opponent.setPoints(opponent.getPoints() - 2);
	}

	public boolean inGame() {
		return phase == DeepestState.INGAME;
	}

	public void handleDisconnect(boolean isReachable) {
		phase = DeepestState.LOBBY;
		if (isReachable && gameStarted) {
			setGameOver(false);
			status.setText(opponent.getUsername() + " has disconnected");
			disconnected = true;

		} else
			status.setText("Server can't be reached,\n Check your Internet Settings");
		status.setBounds(640 - (.5f * status.getPrefWidth()),
				340 - (.5f * status.getPrefHeight()), status.getPrefWidth(),
				status.getPrefHeight());
		stage.addActor(status);
		player.remove();
		opponent.remove();
		PC.remove();
		timer.remove();
		enemyReady.remove();
		playerReady.remove();
		roomId.remove();
		for (Button b : Colors) {
			b.remove();
		}
		stage.addActor(return_MainMenu);
	}

	public void handleBlind(boolean me) {
		if (me) {
			player.addAction(sequence(hide(), delay(2.5f, show())));
			stage.addActor(smileyPlayer);
			smileyPlayer.addAction(delay(2.5f, removeActor()));
			opponent.setPoints(opponent.getPoints() - 1);
		} else {
			opponent.addAction(sequence(hide(), delay(2.5f, show())));
			stage.addActor(smileyOp);
			smileyOp.addAction(delay(2.5f, removeActor()));
		}
	}

	public void handleLoginReject(String reason) {
		phase = DeepestState.LOBBY;
		status.setText(reason);
		stage.addActor(status);
		player.remove();
		opponent.remove();
		PC.remove();
		timer.remove();
		enemyReady.remove();
		playerReady.remove();
		roomId.remove();
		for (Button b : Colors) {
			b.remove();
		}
		stage.addActor(return_MainMenu);

	}

	public void dispose() {
		player.dispose();

	}

	public void stopCountdown() {
		timer.remove();
		phase = DeepestState.LOBBY;
	}

}
