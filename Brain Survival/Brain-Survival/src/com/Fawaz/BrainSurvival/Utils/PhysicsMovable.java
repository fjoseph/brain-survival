package com.Fawaz.BrainSurvival.Utils;

public interface PhysicsMovable {

	public void update();

	public void setStep();
	
	public void dispose();
}
