package com.Fawaz.BrainSurvival;

public interface ThirdPartyInterface {
	public void Login();

	// get if client is signed in to Google+
	public boolean getSignedIn();

	// submit a score to a leaderboard
	public void submitScore(boolean isMultiplayer, int score);

	// gets the scores and displays them through googles default widget
	public void showLeaderBoards();

	// gets the score and gives access to the raw score data

	// display achievements
	public void displayAchievements();

	// unlock achievements
	public void unlockAchievements(int index);

	// show Add
	public void showAds();
}
