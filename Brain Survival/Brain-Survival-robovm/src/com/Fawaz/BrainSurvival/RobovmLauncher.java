package com.Fawaz.BrainSurvival;

import java.util.ArrayList;

import org.robovm.apple.foundation.NSArray;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSString;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.bindings.adcolony.AdColony;
import org.robovm.bindings.adcolony.AdColonyAdDelegate;
import org.robovm.bindings.adcolony.AdColonyDelegate;
import org.robovm.bindings.gamecenter.GameCenterManager;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class RobovmLauncher extends IOSApplication.Delegate implements
		ThirdPartyInterface, AdColonyDelegate, AdColonyAdDelegate {

	IOSApplication iosApp;
	private BrainSurvival game;
	private GameCenterManager gcManager;
	private boolean signedIn = false;
	final static String APP_ID = "app96a880af78c946b6a2";
	final static String ZONE_ID = "vzdfe1ba1daa974e2daf";

	

	@Override
	protected IOSApplication createApplication() {
		IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		config.orientationLandscape = true;
		config.orientationPortrait = false;
		game = new BrainSurvival(this);
		ArrayList<NSString> aZones = new ArrayList<NSString>();
		aZones.add(new NSString(ZONE_ID));
		NSArray<NSString> zones = new NSArray<NSString>(aZones);
		AdColony.configure(APP_ID, zones, this, true);
		iosApp = new IOSApplication(game, config);
		return iosApp;
	}

	public static void main(String[] argv) {
		NSAutoreleasePool pool = new NSAutoreleasePool();
		UIApplication.main(argv, null, RobovmLauncher.class);		
		pool.close();
	}

	@Override
	public void showAds() {
		AdColony.playVideoAd(ZONE_ID, this);
	}

	@Override
	public boolean getSignedIn() {
		// TODO Auto-generated method stub
		return signedIn;
	}

	@Override
	public void submitScore(boolean isMultiplayer, int score) {
		// TODO Auto-generated method stub
		score /= 1000;
		String id;
		if (isMultiplayer)
			id = "MTime_Attack";
		else
			id = "Time_Attack";
		gcManager.reportScore(id, score);
	}

	@Override
	public void showLeaderBoards() {
		// TODO Auto-generated method stub
		gcManager.showLeaderboardsView();
	}

	@Override
	public void displayAchievements() {
		// TODO Auto-generated method stub
		gcManager.showAchievementsView();
	}

	@Override
	public void unlockAchievements(int index) {
		// TODO Auto-generated method stub
		String identifier = "ach" + ++index;

		gcManager.reportAchievement(identifier);

	}

	@Override
	public void onAdStartedInZone(String zoneID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAdAttemptFinished(boolean shown, String zoneID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAdAvailabilityChange(boolean available, String zoneID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onV4VCReward(boolean success, String currencyName, int amount,
			String zoneID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void Login() {
		// TODO Auto-generated method stub
		
	}

}
