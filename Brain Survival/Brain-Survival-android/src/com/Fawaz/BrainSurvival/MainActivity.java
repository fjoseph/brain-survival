package com.Fawaz.BrainSurvival;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Intent;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyVideoAd;

public class MainActivity extends AndroidApplication implements
		GameHelperListener, ThirdPartyInterface {

	private BrainSurvival game;
	private GameHelper aHelper;
	private String account, scope;
	final static String APP_ID = "app6965435463c5496c8c";
	final static String ZONE_ID = "vz4936071bdaf0421084";

	public MainActivity() {
		aHelper = new GameHelper(this);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = false;
		AdColony.configure(this, "version:1.5.4,store:google", APP_ID, ZONE_ID);
		game = new BrainSurvival(this);
		aHelper.setup(this);
		initialize(game, cfg);

	}

	public void showAds() {
		AdColonyVideoAd ad = new AdColonyVideoAd();
		ad.show();
	}

	@Override
	public void onStart() {
		super.onStart();
		aHelper.onStart(this);

	}

	@Override
	public void onStop() {
		super.onStop();
		aHelper.onStop();
	}

	@Override
	protected void onPause() {
		super.onPause();
		AdColony.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		AdColony.resume(this);
	}

	@Override
	public void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		aHelper.onActivityResult(request, response, data);
	}

	public void onSignInFailed() {
		System.out.println("sign in failed");
	}

	public void onSignInSucceeded() {
		System.out.println("sign in succeeded");
		//resetAchievements();
	}

	public void Login() {
		try {
			runOnUiThread(new Runnable() {

				// @Override
				public void run() {
					aHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception ex) {

		}
	}

	public boolean getSignedIn() {
		return aHelper.isSignedIn();
	}

	@Override
	public void showLeaderBoards() {
		startActivityForResult(aHelper.getGamesClient()
				.getAllLeaderboardsIntent(), 105);
	}

	@Override
	public void displayAchievements() {
		startActivityForResult(
				aHelper.getGamesClient().getAchievementsIntent(), 500);
	}

	@Override
	public void unlockAchievements(int index) {
		String achievementId = null;
		switch (index) {
		case 0:
			achievementId = getString(R.string.achievement_start_a_time_attack);
			break;
		case 1:
			achievementId = getString(R.string.achievement_30_seconds_);
			break;
		case 2:
			achievementId = getString(R.string.achievement_1_minute);
			break;
		case 3:
			achievementId = getString(R.string.achievement_2_minutes);
			break;
		case 4:
			achievementId = getString(R.string.achievement_get_in_a_public_game);
			break;
		case 5:
			achievementId = getString(R.string.achievement_win_a_game);
			break;
		case 6:
			achievementId = getString(R.string.achievement_a_rivalry_beginning_);
			break;
		case 7:
			achievementId = getString(R.string.achievement_you_are_cordially_invited);
		}
		aHelper.getGamesClient().unlockAchievement(achievementId);
	}

	public void resetAchievements() {
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				account = aHelper.getGamesClient().getCurrentAccountName();
				scope = aHelper.getScopes();
				String token = "";
				try {
					token = GoogleAuthUtil.getToken(MainActivity.this, account,
							scope);
				} catch (UserRecoverableAuthException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (GoogleAuthException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String[] ids = getResources()
						.getStringArray(R.array.idsToReset);

				HttpClient client = new DefaultHttpClient();
				for (String id : ids) {
					HttpPost post = new HttpPost("https://www.googleapis.com"
							+ "/games/v1management" + "/achievements/" + id
							+ "/reset" + "?access_token=" + token);

					try {
						client.execute(post);
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};

		new Thread(runnable).start();

	}

	@Override
	public void submitScore(boolean isMultiplayer, int score) {
		String leaderboardID = null;
		if (isMultiplayer)
			leaderboardID = getString(R.string.leaderboard_multiplayer_time_attack);
		else
			leaderboardID = getString(R.string.leaderboard_time_attack);
		aHelper.getGamesClient().submitScore(leaderboardID, score);

	}

}