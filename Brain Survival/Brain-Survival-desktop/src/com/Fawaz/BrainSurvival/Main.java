package com.Fawaz.BrainSurvival;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main implements ThirdPartyInterface {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		Main main = new Main();
		cfg.title = "Brain-Survival";
		cfg.useGL20 = false;
		cfg.width = 720;
		cfg.height = 550;
		
		new LwjglApplication(new BrainSurvival(main), cfg);
	}

	@Override
	public void showAds() {
		
		
	}

	@Override
	public void Login() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getSignedIn() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void submitScore(boolean isMultiplayer, int score) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showLeaderBoards() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayAchievements() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unlockAchievements(int index) {
		// TODO Auto-generated method stub
		
	}

}